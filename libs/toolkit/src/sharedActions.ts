import { createAction } from '@reduxjs/toolkit';

export const logoutAction = createAction('logout');
export const addressSwitchAction = createAction<{
  networkId?: number;
}>('addressSwitch');
export const networkSwitchAction = createAction<{
  networkId?: number;
}>('networkSwitch');
