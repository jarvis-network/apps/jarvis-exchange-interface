import type { AddEthereumChainParameter } from '@web3-react/types';
import { Network } from '@jarvis-network/core-utils/dist/eth/networks';
import { getInfuraEndpoint } from '@jarvis-network/core-utils/dist/apis/infura';

interface BasicChainInformation {
  urls: string[];
  name: string;
}

interface ExtendedChainInformation extends BasicChainInformation {
  nativeCurrency: AddEthereumChainParameter['nativeCurrency'];
  blockExplorerUrls: AddEthereumChainParameter['blockExplorerUrls'];
}

function isExtendedChainInformation(
  chainInformation: BasicChainInformation | ExtendedChainInformation,
): chainInformation is ExtendedChainInformation {
  return !!(chainInformation as ExtendedChainInformation).nativeCurrency;
}

export const CHAINS: {
  [chainId: number]: BasicChainInformation | ExtendedChainInformation;
} = {
  1: {
    urls: [
      getInfuraEndpoint(
        Network.mainnet,
        'https',
        process.env.NEXT_PUBLIC_INFURA_API_KEY,
      ),
    ].filter(url => url !== undefined),
    name: 'Mainnet',
  },
  42: {
    urls: [
      getInfuraEndpoint(
        Network.kovan,
        'https',
        process.env.NEXT_PUBLIC_INFURA_API_KEY,
      ),
    ].filter(url => url !== undefined),
    name: 'Mainnet',
  },
  137: {
    urls: [
      getInfuraEndpoint(
        Network.polygon,
        'https',
        process.env.NEXT_PUBLIC_INFURA_API_KEY,
      ),
    ].filter(url => url !== undefined),
    name: 'Mainnet',
  },
  80001: {
    urls: [
      getInfuraEndpoint(
        Network.mumbai,
        'https',
        process.env.NEXT_PUBLIC_INFURA_API_KEY,
      ),
    ].filter(url => url !== undefined),
    name: 'Mainnet',
  },
  56: {
    urls: [
      getInfuraEndpoint(
        Network.bsc,
        'https',
        process.env.NEXT_PUBLIC_INFURA_API_KEY,
      ),
    ].filter(url => url !== undefined),
    name: 'Mainnet',
  },
  43114: {
    urls: [
      getInfuraEndpoint(
        Network.avalanche,
        'https',
        process.env.NEXT_PUBLIC_INFURA_API_KEY,
      ),
    ].filter(url => url !== undefined),
    name: 'Mainnet',
  },
  100: {
    urls: [
      getInfuraEndpoint(
        Network.gnosis,
        'https',
        process.env.NEXT_PUBLIC_INFURA_API_KEY,
      ),
    ].filter(url => url !== undefined),
    name: 'Mainnet',
  },
};

export function getAddChainParameters(
  chainId: number,
): AddEthereumChainParameter | number {
  const chainInformation = CHAINS[chainId];
  if (isExtendedChainInformation(chainInformation)) {
    return {
      chainId,
      chainName: chainInformation.name,
      nativeCurrency: chainInformation.nativeCurrency,
      rpcUrls: chainInformation.urls,
      blockExplorerUrls: chainInformation.blockExplorerUrls,
    };
  }
  return chainId;
}

export const URLS: { [chainId: number]: string[] } = Object.keys(
  CHAINS,
).reduce<{ [chainId: number]: string[] }>((accumulator, chainId) => {
  const validURLs: string[] = CHAINS[Number(chainId)].urls;

  if (validURLs.length) {
    accumulator[Number(chainId)] = validURLs;
  }

  return accumulator;
}, {});
export const supportedNetworkIds = Object.keys(CHAINS).map(chainId =>
  Number(chainId),
);
