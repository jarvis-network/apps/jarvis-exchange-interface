import { initializeConnector } from '@web3-react/core';
import { MetaMask } from '@web3-react/metamask';

import { supportedNetworkIds } from '../chains';

export const [metaMask, hooks] = initializeConnector<MetaMask>(
  actions => new MetaMask(actions),
  supportedNetworkIds,
);
