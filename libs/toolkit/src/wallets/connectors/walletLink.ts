import { initializeConnector } from '@web3-react/core';

import { WalletLink } from '@web3-react/walletlink';

import { supportedNetworkIds, URLS } from '../chains';

export const [walletLink, walletLinkHooks] = initializeConnector<WalletLink>(
  actions =>
    new WalletLink(actions, {
      url: URLS[1][0],
      appName: 'Jarvis Credit line',
    }),
  supportedNetworkIds,
);
