import { CoinbaseWallet } from '@web3-react/coinbase-wallet';
import { Web3ReactHooks } from '@web3-react/core';
import { MetaMask } from '@web3-react/metamask';
import { WalletConnect } from '@web3-react/walletconnect';
import { Frame } from '@web3-react/frame';
import { WalletLink } from '@web3-react/walletlink';

import { coinbaseWallet, hooks as coinbaseWalletHooks } from './coinbaseWallet';
import { frame, frameHooks } from './frame';

import { hooks as metaMaskHooks, metaMask } from './metaMask';
import { hooks as walletConnectHooks, walletConnect } from './walletConnect';
import { walletLink, walletLinkHooks } from './walletLink';

const connectors: [
  MetaMask | WalletConnect | CoinbaseWallet | WalletLink | Frame,
  Web3ReactHooks,
][] = [
  [metaMask, metaMaskHooks],
  [walletConnect, walletConnectHooks],
  [coinbaseWallet, coinbaseWalletHooks],
  [walletLink, walletLinkHooks],
  [frame, frameHooks],
];
export type ConnectorType = typeof connectors;
export default connectors;
