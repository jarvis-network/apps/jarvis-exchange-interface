import { initializeConnector } from '@web3-react/core';
import { Frame } from '@web3-react/frame';

import { supportedNetworkIds } from '../chains';

export const [frame, frameHooks] = initializeConnector<Frame>(
  actions => new Frame(actions, undefined, false),
  supportedNetworkIds,
);
