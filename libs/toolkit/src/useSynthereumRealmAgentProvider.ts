import { RealmAgent } from '@jarvis-network/synthereum-ts/dist/core/realm-agent';
import { context$ } from '@jarvis-network/synthereum-ts/dist/epics/types';
import { useEffect } from 'react';
import type { BehaviorSubject } from 'rxjs';

export function useSynthereumRealmAgentProvider({
  synthereumRealmAgent$,
}: {
  synthereumRealmAgent$: BehaviorSubject<RealmAgent | null>;
}): void {
  useEffect(() => {
    context$.subscribe(context => {
      if (context && context.poolRealmAgent) {
        synthereumRealmAgent$.next(context!.poolRealmAgent);
      }
    });
    return () => {
      synthereumRealmAgent$.next(null);
    };
  }, []);
}
