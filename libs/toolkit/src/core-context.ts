import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/dist';
import {
  createContext,
  updateAddress,
} from '@jarvis-network/synthereum-ts/dist/epics/core';
import { context$ } from '@jarvis-network/synthereum-ts/dist/epics/types';
import type { Web3Provider } from '@ethersproject/providers';

export const dependencies = {
  context$,
};

export const newWeb3Context = async (
  web3: Web3Provider,
  connectorName: string | null,
  networkId: number | undefined,
  cancel = false,
) => {
  if (cancel) {
    context$.next(null);
    return;
  }

  if (web3) {
    const nodeId = (await web3.getNetwork()).chainId;
    if (networkId === nodeId) {
      context$.next(await createContext(web3, connectorName));
    } else {
      throw new Error('Reload');
    }
  }
};

export const onAddressUpdate = async (
  address: AddressOn<SupportedNetworkName> | undefined,
) => {
  if (address) {
    context$.next(await updateAddress(address));
  } else {
    context$.next(null);
  }
};
