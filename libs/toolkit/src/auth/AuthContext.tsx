import React, { createContext, ReactNode, useContext, useMemo } from 'react';
import { AnyAction } from 'redux';
import { useWeb3React, Web3ReactProvider } from '@web3-react/core';

import { Connector } from '@web3-react/types';

import connectors from '../wallets/connectors';

export type LogoutAction = () => AnyAction;

interface Auth {
  login(connector: Connector): Promise<void>;

  logout(): void;
}

type Web3ReactHookResult = ReturnType<typeof useWeb3React>;
type Web3ReactConnector = Web3ReactHookResult['connector'];

function authFactory(connector: Web3ReactConnector): Auth {
  return {
    login(_connector) {
      return _connector.activate();
    },
    logout() {
      connector.deactivate();
      localStorage.removeItem('jarvis/auto-login');
    },
  } as Auth;
}

const AuthContext = createContext<Auth | null>(null);

interface Props {
  children: ReactNode;
}

export function AuthProvider(props: Props): JSX.Element {
  return (
    <Web3ReactProvider connectors={connectors} lookupENS={false}>
      <AuthContextProvider {...props} />
    </Web3ReactProvider>
  );
}

export function AuthContextProvider({ children }: Props): JSX.Element {
  const { connector } = useWeb3React();

  const value = useMemo(() => authFactory(connector), [connector]);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}

export function useAuth(): Auth {
  const context = useContext(AuthContext);
  if (!context)
    throw new Error(
      'AuthContext not provided. Use `AuthProvider` from `@jarvis-network/app-toolkit`.',
    );
  return context;
}
