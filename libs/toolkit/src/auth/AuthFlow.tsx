import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { AnyAction } from 'redux';
import { useDispatch, useSelector } from 'react-redux';
import {
  Modal,
  styled,
  useNotifications,
  useIsMobile,
  noop,
  NotificationType,
  NotificationTypeWithOptions,
} from '@jarvis-network/ui';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { SupportedNetworkName } from '@jarvis-network/synthereum-config/dist';

import { useWeb3React } from '@web3-react/core';

import { Connector } from '@web3-react/types';

import { MetaMask } from '@web3-react/metamask';

import { WalletConnect } from '@web3-react/walletconnect';

import { CoinbaseWallet } from '@web3-react/coinbase-wallet';

import { WalletLink } from '@web3-react/walletlink';

import { Frame } from '@web3-react/frame';

import { newWeb3Context, onAddressUpdate } from '../core-context';
import { Context, useSubjects } from '../CoreObservablesContext';
import { useSynthereumRealmAgentProvider } from '../useSynthereumRealmAgentProvider';

import {
  logoutAction,
  networkSwitchAction,
  addressSwitchAction,
} from '../sharedActions';

import { usePrevious } from '../usePrevious';

import { metaMask } from '../wallets/connectors/metaMask';

import { walletConnect } from '../wallets/connectors/walletConnect';

import { coinbaseWallet } from '../wallets/connectors/coinbaseWallet';

import { walletLink } from '../wallets/connectors/walletLink';

import { frame } from '../wallets/connectors/frame';

import { WalletPicker } from './WalletPicker';
import { useAuth } from './AuthContext';
import { supportedNetworkIds } from './env';
import { UnsupportedNetwork } from './UnsupportedNetwork';

const ModalWrapper = styled.div`
  @media screen and (max-width: ${props =>
      props.theme.rwd.breakpoints[props.theme.rwd.desktopIndex - 1]}px) {
    .auth-modal {
      justify-content: flex-end;
      background: none;

      > * {
        height: auto;
        padding-bottom: 30px;
      }
    }
  }
`;

interface PageProps {
  onNext(): void;

  onPrev(): void;
}

type Page = React.ComponentClass<PageProps> | React.FC<PageProps>;

type AppState = {
  app: { isAuthModalVisible: boolean };
};

interface Props {
  appName: string;
  // eslint-disable-next-line react/require-default-props
  notify?: (
    notify: ReturnType<typeof useNotifications>,
    isMobile: boolean,
    text: string,
    type?: NotificationTypeWithOptions,
    time?: number,
  ) => void;
  setAuthModalVisibleAction: (isVisible: boolean) => AnyAction;
  Welcome: Page;
  Terms: Page;
  ServiceSelect: Page;
  // eslint-disable-next-line react/require-default-props

  // eslint-disable-next-line react/require-default-props
  subjects: ReturnType<typeof useSubjects>;
}

const connectorMapper: any = {
  metaMask,
  walletConnect,
  coinbaseWallet,
  walletLink,
  frame,
};

function getConnectorName(connector: Connector) {
  if (connector instanceof MetaMask) {
    return 'MetaMask';
  }
  if (connector instanceof WalletConnect) {
    return 'WalletConnect';
  }

  if (connector instanceof CoinbaseWallet) {
    return 'CoinbaseWallet';
  }

  if (connector instanceof WalletLink) {
    return 'WalletLink';
  }

  if (connector instanceof Frame) {
    return 'Frame';
  }

  return 'Unknown';
}

function getConnectorConnection(name: string) {
  name = name.toLowerCase();

  if (name === 'walletconnect') {
    return walletConnect;
  }
  if (name === 'coinbasewallet') {
    return coinbaseWallet;
  }
  if (name === 'walletlink') {
    return walletLink;
  }
  if (name === 'frame') {
    return frame;
  }

  return metaMask;
}
export function AuthFlow({
  appName,
  notify,
  setAuthModalVisibleAction,
  Welcome,
  Terms,
  ServiceSelect,

  subjects,
}: Props): JSX.Element {
  const {
    isActive,
    account,
    chainId: networkId,
    provider,
    connector,
  } = useWeb3React();
  const dispatch = useDispatch();
  const previousActive = usePrevious(isActive);

  useEffect(() => {
    const autoLogin = localStorage.getItem('jarvis/auto-login');

    if (autoLogin && connector && connector.connectEagerly) {
      getConnectorConnection(autoLogin).connectEagerly?.();
    }
  }, []);

  useEffect(() => {
    if (isActive) {
      const walletName = getConnectorName(connector);

      if (walletName) {
        localStorage.setItem('jarvis/auto-login', walletName);
      }
      return;
    }

    if (!previousActive) return;

    localStorage.removeItem('jarvis/auto-login');
    dispatch(logoutAction());
  }, [isActive, previousActive, dispatch, connector]);

  const previousNetworkId = usePrevious(networkId);
  useEffect(() => {
    if (!previousNetworkId && !networkId) {
      dispatch(networkSwitchAction({ networkId: 137 }));
    }
    if (!previousNetworkId && networkId) return;
    if (previousNetworkId === networkId) return;
    dispatch(networkSwitchAction({ networkId }));
  }, [previousNetworkId, networkId, dispatch]);

  const previousAddress = usePrevious(account);
  useEffect(() => {
    if (!previousAddress && account) return;
    if (previousAddress === account) return;
    dispatch(addressSwitchAction({ networkId }));
  }, [previousAddress, account, dispatch]);

  const auth = useAuth();

  const notifyFn = useNotifications();
  const isMobile = useIsMobile();

  const postNotificationRef = useRef<
    (title: string, options?: NotificationTypeWithOptions) => void
  >(noop);
  useMemo(() => {
    postNotificationRef.current = (
      title: string,
      options?: NotificationTypeWithOptions,
    ) => {
      if (notify) {
        notify(notifyFn, isMobile, title, options);
      } else {
        notifyFn(title, options);
      }
    };
  }, [isMobile, notify, notifyFn]);

  const isAuthModalVisible = useSelector<AppState, boolean>(
    state => state.app.isAuthModalVisible,
  );

  const [current, setPage] = useState(0);
  const next = () => {
    if (current === 1) {
      localStorage.setItem(`${appName}/tos-accepted`, 'true');
    }
    setPage(p => p + 1);
  };
  const prev = () => setPage(p => p - 1);

  const pages = [Welcome, Terms, ServiceSelect, WalletPicker];
  const Page = pages[current];

  useEffect(() => {
    async function update() {
      // just logged in
      if (!previousNetworkId) {
        if (account) {
          const showNotification = () =>
            postNotificationRef.current('You have successfully signed in', {
              type: NotificationType.success,
              icon: '👍🏻',
            });
          try {
            if (!provider) {
              return;
            }
            await newWeb3Context(
              provider,
              localStorage.getItem('jarvis/auto-login'),
              networkId,
            );
            onAddressUpdate(account as AddressOn<SupportedNetworkName>);
            dispatch(networkSwitchAction({ networkId }));
            console.log('setCurrent->1');
            showNotification();
          } catch (e: any) {
            if (e.message === 'Need to Login again') {
              auth.logout();
            }
            if (e.message === 'Reload') {
              // eslint-disable-next-line no-restricted-globals
              location.reload();
            }

            if (!isActive) {
              alert('Unsupported network on connected wallet');
              console.log('1', e);
            }
          }
          return;
        }
      }
      // just logged out
      if (!networkId && !isActive) {
        onAddressUpdate(undefined);

        dispatch(networkSwitchAction({ networkId: 137 }));
        console.log('setCurrent->2');
        return;
      }
      // address has changed
      if (previousNetworkId === networkId) {
        try {
          onAddressUpdate(account as AddressOn<SupportedNetworkName>);
          dispatch(networkSwitchAction({ networkId }));
          console.log('setCurrent->3');
        } catch (e: any) {
          if (e.message === 'Need to Login again') {
            auth.logout();
          }
          if (e.message === 'Reload') {
            // eslint-disable-next-line no-restricted-globals
            location.reload();
          }
          console.log('2', e);
        }

        return;
      }
      // network changed
      postNotificationRef.current('You have switched your network', {
        type: NotificationType.success,
        icon: '⚡️',
      });
      try {
        if (!provider) {
          return;
        }
        await newWeb3Context(
          provider,
          localStorage.getItem('jarvis/auto-login'),
          networkId,
        );
        onAddressUpdate(account as AddressOn<SupportedNetworkName>);
        console.log('setCurrent->4');
      } catch (e: any) {
        if (e?.message === 'Need to Login again') {
          auth.logout();
        }
        if (e?.message === 'Reload') {
          // eslint-disable-next-line no-restricted-globals
          location.reload();
        }
        console.log('4', e);
      }
    }

    update();
  }, [account, previousNetworkId, networkId]);

  useEffect(() => {
    if (isAuthModalVisible) {
      if (localStorage.getItem(`${appName}/tos-accepted`) === 'true') {
        setPage(2);
        return;
      }
      setPage(0);
    }
  }, [appName, isAuthModalVisible]);

  const handleClose = useCallback(() => {
    dispatch(setAuthModalVisibleAction(false));
  }, [dispatch, setAuthModalVisibleAction]);

  useEffect(() => {
    const autoLoginWallet = localStorage.getItem(`jarvis/auto-login`);

    if ((window as any)?.ethereum?.isTrust) {
      auth.login(metaMask);
      return;
    }
    if (!autoLoginWallet || !connectorMapper[autoLoginWallet]) {
      return;
    }
    auth.login(connectorMapper[autoLoginWallet]);
  }, [auth]);

  useEffect(() => {
    if (provider) {
      dispatch(setAuthModalVisibleAction(false));
    }
  }, [dispatch, setAuthModalVisibleAction, provider]);
  const isUnsupportedNetworkId =
    networkId && !supportedNetworkIds.includes(networkId);

  return (
    <>
      <Providers subjects={subjects} />
      <ModalWrapper>
        <Modal
          isOpened={isUnsupportedNetworkId || isAuthModalVisible}
          onClose={handleClose}
          overlayStyle={{ zIndex: 3 }}
          overlayClassName="auth-modal"
        >
          {isUnsupportedNetworkId ? (
            <UnsupportedNetwork
              handleDismiss={() => {
                auth.logout();
                handleClose();
              }}
              handleSwitchWallet={() => {
                // event.preventDefault();
                setPage(3);
                auth.logout();
              }}
            />
          ) : (
            Page && <Page onNext={next} onPrev={prev} />
          )}
        </Modal>
      </ModalWrapper>
    </>
  );
}

function Providers({ subjects }: { subjects: Context }) {
  useSynthereumRealmAgentProvider({
    ...subjects,
  });

  return null;
}
