import { useWeb3React } from '@web3-react/core';

export function useWeb3() {
  return useWeb3React();
}
