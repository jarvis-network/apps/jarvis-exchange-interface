import { parseSupportedNetworkId } from '@jarvis-network/synthereum-config';
import { getInfuraWeb3 } from '@jarvis-network/core-utils/dist/apis/infura';

import { SynthereumRealmWithWeb3 } from '../../core/types/realm';

import { loadRealm } from '../../core/realms/synthereum/load';

import { querySynthereumPriceFeed } from './index';

describe('query Synthereum Price Feed', () => {
  let realm: SynthereumRealmWithWeb3;
  const unsupportedPair = 'jNGN/USDC';
  const supportedPair = 'jEUR/USDC';

  beforeAll(async () => {
    const netId = parseSupportedNetworkId(1);
    const web3 = getInfuraWeb3(netId);
    realm = await loadRealm(web3, netId);
  });
  it('Should return null if pair is not supported', async () => {
    const pairPrice = await querySynthereumPriceFeed(realm, unsupportedPair);
    expect(pairPrice).toBe(null);
  });
  it('Should pair price be defined if pair is supported', async () => {
    const pairPrice = await querySynthereumPriceFeed(realm, supportedPair);
    expect(pairPrice).toBeDefined();
  });
});
