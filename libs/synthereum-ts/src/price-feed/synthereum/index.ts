import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import type {
  AnySynthereumPair,
  SupportedNetworkName,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config';
import { ISynthereumPriceFeed } from '@jarvisnetwork/synthereum-contracts/build/typechain';
import { ContractInstance } from 'libs/core-utils/dist/eth/contracts/types';
import { stringToHex } from 'web3-utils';

interface Realm {
  readonly priceFeed: ContractInstance<
    SupportedNetworkName,
    ISynthereumPriceFeed
  >;
}

export const serializePair = (pair: AnySynthereumPair): string => {
  const [input] = pair.split('/') as [SupportedSynthereumSymbol];
  return stringToHex(`${input.slice(1)}USD`);
};
export const serializePair2 = (pair: AnySynthereumPair): string => {
  const [input] = pair.split('/') as [SupportedSynthereumSymbol];
  return `${input.slice(1)}USD`;
};
export async function querySynthereumPriceFeed(
  realm: Realm,
  pair: AnySynthereumPair,
): Promise<StringAmount | null> {
  try {
    return (await realm.priceFeed.instance.methods
      .getLatestPrice(serializePair(pair))
      .call()) as StringAmount;
  } catch (err) {
    return null;
  }
}
