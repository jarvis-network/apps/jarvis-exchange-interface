import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import {
  SupportedNetworkId,
  SupportedNetworkName,
} from '@jarvis-network/synthereum-config';
import { ethers } from 'ethers';
import { Provider } from 'ethers-multicall';
import { Web3On } from 'libs/core-utils/dist/eth/web3-instance';
import type { Epic as RoEpic } from 'redux-observable';
import { BehaviorSubject, Observable } from 'rxjs';

import { RealmAgent } from '../core/realm-agent';
import { SynthereumRealmWithWeb3 } from '../core/types/realm';

export interface Context {
  networkId: SupportedNetworkId | null;
  web3: Web3On<SupportedNetworkName> | null;
  ethersProvider: ethers.providers.Web3Provider | null;
  poolRealmAgent: RealmAgent | null;
  poolRealm: SynthereumRealmWithWeb3 | null;
  agentAddress: AddressOn<SupportedNetworkName> | null;
  connectorName: string | null;
  ethcallProvider: Provider | null;
  active: boolean;
}

export const context$ = new BehaviorSubject<Context | null>({
  web3: null,
  networkId: null,
  poolRealmAgent: null,
  poolRealm: null,
  agentAddress: null,
  ethersProvider: null,
  connectorName: 'inject',
  ethcallProvider: null,
  active: false,
});
export type ReduxAction<Type extends string = string, Payload = any> = {
  type: Type;
  payload?: Payload;
};

export interface StateObservable<S> extends Observable<S> {
  value: S;
}

export type Epic<
  Input extends ReduxAction,
  Output extends Input,
  State = unknown
> = RoEpic<Input, Output, State, Dependencies>;

export interface Dependencies {
  context$: BehaviorSubject<Context> | null;
}
