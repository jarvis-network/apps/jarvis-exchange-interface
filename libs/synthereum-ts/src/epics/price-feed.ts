import { includes } from '@jarvis-network/core-utils/dist/base/array-fp-utils';
import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import {
  AnySynthereumPair,
  ExchangeSynthereumToken,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config';
import { synthereumCollateralSymbols } from '@jarvis-network/synthereum-config/dist/types/price-feed-symbols';
import isEqual from 'deep-equal';

import {
  distinctUntilChanged,
  filter,
  map,
  mapTo,
  switchMap,
  takeUntil,
} from 'rxjs';

import { querySynthereumPriceFeed } from '../price-feed/synthereum';

import { dynamicInterval } from './interval';
import { Epic, ReduxAction } from './types';

export type PriceFeedSymbols = AnySynthereumPair;

export type SymbolPrices = [ExchangeSynthereumToken, StringAmount][];
export type OutputAction = ReduxAction<'UPDATE_PRICES', SymbolPrices>;
export type InputAction =
  | ReduxAction<'UPDATE_PAIRS', PriceFeedSymbols[]>
  | ReduxAction<'UPDATE_INTERVAL', number>
  | OutputAction;

export interface ReduxState {
  assets: {
    [key in ExchangeSynthereumToken]?: { symbol: ExchangeSynthereumToken };
  };
}

export const { changeInterval, interval$ } = dynamicInterval(5000);

export const priceFeedEpic: Epic<ReduxAction, ReduxAction, ReduxState> = (
  action$,
  state$,
  { context$ },
) => {
  action$.subscribe(action => {
    if (action.type === 'UPDATE_INTERVAL') {
      changeInterval(action.payload!);
    }
  });

  return state$
    .pipe(
      map(state =>
        Object.values(state.assets)
          .filter(asset => !includes(synthereumCollateralSymbols, asset.symbol))
          .map(
            asset =>
              `${asset.symbol as SupportedSynthereumSymbol}/USDC` as const,
          ),
      ),
      distinctUntilChanged<PriceFeedSymbols[]>(isEqual),
    )
    .pipe(
      switchMap(pairs => interval$!.pipe(mapTo(pairs))),
      switchMap(pairs =>
        context$!.pipe(
          map(core => ({ core, pairs })),
          takeUntil(
            action$.pipe(
              filter(
                a =>
                  a.type === 'logout' ||
                  a.type === 'addressSwitch' ||
                  a.type === 'networkSwitch',
              ),
            ),
          ),
        ),
      ),
      filter(({ core }) => (core ? !!core.poolRealm?.priceFeed : false)),
      switchMap(async ({ pairs, core }) => {
        if (core) {
          try {
            // const calls = [] as any;

            // if (core.connectorName !== 'injected') {
            //   pairs.forEach((pair: AnySynthereumPair) => {
            //     calls.push({
            //       ethCall: core.poolRealm?.priceFeed.instance.methods.getLatestPrice(
            //         serializePair(pair),
            //       ).call,
            //     });
            //   });
            //   const prices = await makeBatchRequest(core.web3, calls);

            //   return await Promise.all(
            //     pairs.map((p, index: number) => [
            //       p.split('/')[0],
            //       prices[index] as StringAmount,
            //     ]),
            //   );
            // }
            return await Promise.all(
              pairs.map(async p => [
                p.split('/')[0],
                (await querySynthereumPriceFeed(core.poolRealm!, p))!,
              ]),
            );
          } catch (error) {
            console.log('Unable to load prices try one by one');
          }
        }
        return undefined;
      }),

      map(results => ({
        type: 'assets/setCurrentPrice',
        payload: results,
      })),
    );
};
