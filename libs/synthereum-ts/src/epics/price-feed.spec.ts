/* eslint-disable */
import {
  parseSupportedNetworkId,
  SupportedNetworkId,
} from '@jarvis-network/synthereum-config';
import { getInfuraWeb3 } from '@jarvis-network/core-utils/dist/apis/infura';

import { BehaviorSubject, ReplaySubject } from 'rxjs';

import { InputAction, priceFeedEpic, ReduxState } from './price-feed';
import { createContext } from './core';
import { Context, Dependencies } from './types';
import { StateObservable } from 'redux-observable';
import { wei } from '@jarvis-network/core-utils/dist/base/big-number';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';

describe('price-feed epic', () => {
  let context: Context;
  let deps: Dependencies = {
    context$: null,
  };
  let stateInput$: BehaviorSubject<ReduxState>;
  let state$: StateObservable<ReduxState>;
  beforeAll(async () => {
    const netId = parseSupportedNetworkId(1);
    const web3 = getInfuraWeb3(netId);
    context = await createContext(
      web3,
      '' as AddressOn<SupportedNetworkId>,
      true,
    );
    deps = {
      context$: new BehaviorSubject<Context>(context),
    };
    stateInput$ = new BehaviorSubject<ReduxState>({ assets: {} });
    state$ = new StateObservable<ReduxState>(stateInput$, { assets: {} });
  });

  it('Should get single pair jGBP/USDC', async done => {
    const action$ = new ReplaySubject<InputAction>(20);
    action$.next({ type: 'UPDATE_INTERVAL', payload: 1000 });
    stateInput$.next({ assets: { jGBP: { symbol: 'jGBP' } } });
    const priceFeedStream$ = priceFeedEpic(action$, state$!, deps);
    const feed = priceFeedStream$.subscribe({
      next: v => {
        expect(v.payload.length).toEqual(1);
        expect(v.payload[0][0]).toEqual('jGBP');
        expect(wei(v.payload[0][1]).gt(wei(0))).toBeTruthy();
      },
    });
    setTimeout(() => {
      feed.unsubscribe();
      done();
    }, 2000);
  }, 6000);

  it('Should get multiple pairs jEUR/USDC, jGBP/USDC, jCHF/USDC', async done => {
    const action$ = new ReplaySubject<InputAction>(20);
    action$.next({ type: 'UPDATE_INTERVAL', payload: 1000 });
    stateInput$.next({
      assets: {
        jEUR: { symbol: 'jEUR' },
        jGBP: { symbol: 'jGBP' },
        jCHF: { symbol: 'jCHF' },
      },
    });
    const priceFeedStream$ = priceFeedEpic(action$, state$!, deps);
    const feed = priceFeedStream$.subscribe({
      next: v => {
        expect(v.payload.length).toEqual(3);
        expect(wei(v.payload[0][1]).gt(wei(0))).toBeTruthy();
        expect(v.payload!.map(([key]: [string]) => key).sort()).toEqual(
          ['jEUR', 'jGBP', 'jCHF'].sort(),
        );
      },
    });
    setTimeout(() => {
      feed.unsubscribe();
      done();
    }, 2000);
  }, 6000);

  it('Should switch between pairs jEUR/USDC, jGBP/USDC', async done => {
    const action$ = new ReplaySubject<InputAction>(20);
    action$.next({ type: 'UPDATE_INTERVAL', payload: 1000 });
    stateInput$.next({ assets: { jEUR: { symbol: 'jEUR' } } });
    const priceFeedStream$ = priceFeedEpic(action$, state$!, deps);
    const feed = priceFeedStream$.subscribe({
      next: v => {
        expect(v.payload.length).toEqual(1);
        expect(wei(v.payload[0][1]).gt(wei(0))).toBeTruthy();
      },
    });
    setTimeout(() => {
      stateInput$.next({ assets: { jGBP: { symbol: 'jGBP' } } });
    }, 3000);
    setTimeout(() => {
      feed.unsubscribe();
      done();
    }, 6000);
  }, 6000);
});
