import { includes } from '@jarvis-network/core-utils/dist/base/array-fp-utils';
import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { scaleTokenAmountToWei } from '@jarvis-network/core-utils/dist/eth/contracts/erc20';
import {
  ExchangeSelfMintingToken,
  ExchangeSynthereumToken,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config';
import { synthereumCollateralSymbols } from '@jarvis-network/synthereum-config/dist/types/price-feed-symbols';
import BN from 'bn.js';
import isEqual from 'deep-equal';
import { Contract } from 'ethers-multicall';
import {
  distinctUntilChanged,
  filter,
  map,
  mapTo,
  switchMap,
  takeUntil,
} from 'rxjs';

import { changeInterval, interval$, PriceFeedSymbols } from './price-feed';
import { Epic, ReduxAction } from './types';

export interface WalletBalance {
  asset: ExchangeSelfMintingToken;
  amount: StringAmount;
}

export type OutputAction = ReduxAction<
  'wallet/setWalletBalances',
  WalletBalance[]
>;

type SelectedWalletBalance = ReduxAction<
  'GET_WALLET_BALANCE',
  ExchangeSelfMintingToken[]
>;
export type InputAction = SelectedWalletBalance | OutputAction;

export interface ReduxState {
  assets: {
    [key in ExchangeSynthereumToken]?: { symbol: ExchangeSynthereumToken };
  };
}
export const walletFeedEpic: Epic<ReduxAction, ReduxAction, ReduxState> = (
  action$,
  state$,
  { context$ },
) => {
  action$.subscribe(action => {
    if (action.type === 'UPDATE_INTERVAL') {
      changeInterval(action.payload!);
    }
  });

  return state$
    .pipe(
      map(state =>
        Object.values(state.assets)
          .filter(asset => !includes(synthereumCollateralSymbols, asset.symbol))
          .map(
            asset =>
              `${asset.symbol as SupportedSynthereumSymbol}/USDC` as const,
          ),
      ),
      distinctUntilChanged<PriceFeedSymbols[]>(isEqual),
    )
    .pipe(
      switchMap(pairs => interval$!.pipe(mapTo(pairs))),
      switchMap(pairs =>
        context$!.pipe(
          map(core => ({ core, pairs })),
          takeUntil(
            action$.pipe(
              filter(
                a =>
                  a.type === 'logout' ||
                  a.type === 'addressSwitch' ||
                  a.type === 'networkSwitch',
              ),
            ),
          ),
        ),
      ),
      filter(({ core }) => (core ? !!core.poolRealmAgent?.activePools : false)),
      switchMap(async ({ core }) => {
        if (core) {
          try {
            await core.ethcallProvider!.init();

            const getBalanceCalls = [] as any;
            const balancesResponse: any = [];
            Object.values(core.poolRealmAgent!.activePools).forEach(pool => {
              getBalanceCalls.push(
                new Contract(
                  pool.syntheticToken.address,
                  pool.syntheticToken.instance.options.jsonInterface as any,
                ).balanceOf(core.agentAddress!),
              );
            });
            getBalanceCalls.push(
              new Contract(
                core.poolRealmAgent!.collateralToken.address,
                core.poolRealmAgent!.collateralToken.instance.options
                  .jsonInterface as any,
              ).balanceOf(core.agentAddress!),
            );
            const syntheticBalances = await core.ethcallProvider!.all(
              getBalanceCalls,
            );

            Object.values(core.poolRealmAgent!.activePools).forEach(
              (pool: any, index: number) => {
                balancesResponse.push({
                  symbol: pool.syntheticToken.symbol,
                  balance: scaleTokenAmountToWei({
                    amount: new BN(syntheticBalances[index].toString()),
                    decimals: pool.syntheticToken.decimals,
                  }).toString(),
                });
              },
            );

            balancesResponse.push({
              symbol:
                core.poolRealmAgent!.collateralToken.symbol !== 'WXDAI'
                  ? core.poolRealmAgent!.collateralToken.symbol
                  : 'wXDAI',
              balance: scaleTokenAmountToWei({
                amount: new BN(
                  syntheticBalances[syntheticBalances.length - 1].toString(),
                ),
                decimals: core.poolRealmAgent!.collateralToken.decimals,
              }).toString(),
            });

            return balancesResponse;
          } catch (error) {
            return undefined;
          }
        }
        return undefined;
      }),

      map(results => ({
        type: 'wallet/setBalances',
        payload: results,
      })),
    );
};
