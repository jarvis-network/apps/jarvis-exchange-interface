import {
  poolVersionsFromNetworkId,
  SupportedNetworkName,
} from '@jarvis-network/synthereum-config';
import { distinctUntilChanged, filter, map, switchMap, takeUntil } from 'rxjs';

import { loadRealm, PoolVersionsToLoad } from '../core/realms/synthereum/load';
import { SynthereumRealmWithWeb3 } from '../core/types/realm';

import { Context, Dependencies, Epic, ReduxAction } from './types';
import { ReduxState } from './wallet';

export const loadPoolRealmAgent = async (
  context: Context,
): Promise<SynthereumRealmWithWeb3<SupportedNetworkName> | null> => {
  try {
    const { web3, networkId } = context;
    if (!web3) {
      return null;
    }
    const loadPoolRealm = await loadRealm(
      web3,
      context.ethersProvider!,
      context.ethcallProvider!,
      networkId!,
      poolVersionsFromNetworkId[
        networkId!
      ] as PoolVersionsToLoad<SupportedNetworkName>,
    );

    return loadPoolRealm;
  } catch (error) {
    console.log('I should reload');
  }
  // return null;
  return loadPoolRealmAgent(context);
};

export const realmFrontendEpic: Epic<ReduxAction, ReduxAction, ReduxState> = (
  action$,
  state$,
  { context$ }: Dependencies,
) =>
  action$.pipe(
    filter(action => action.type === 'INIT_CORE_CONTEXT'),
    distinctUntilChanged(),
    switchMap(_ =>
      context$!.pipe(
        map(context => ({ context })),
        takeUntil(
          action$.pipe(
            filter(
              a => a.type === 'networkSwitch' || a.type === 'addressSwitch',
            ),
          ),
        ),
      ),
    ),
    switchMap(async ({ context }) => {
      if (context === null)
        return {
          type: 'app/contextUpdate',
          payload: undefined,
        };
      const { web3, networkId, agentAddress, poolRealm } = context;
      if (agentAddress && web3 && networkId && poolRealm === null) {
        await loadPoolRealmAgent(context);
      }

      return {
        type: 'app/contextUpdate',
        payload: {
          networkId: 1,
          address: '0xAsim',
        },
      };
    }),
  );
