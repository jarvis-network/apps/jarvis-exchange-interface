import { distinctUntilChanged, filter, map, switchMap, takeUntil } from 'rxjs';

import { Dependencies, Epic, ReduxAction } from './types';
import { ReduxState } from './wallet';

export const getFeePercentageEpic: Epic<
  ReduxAction,
  ReduxAction,
  ReduxState
> = (action$, state$, { context$ }: Dependencies) =>
  action$.pipe(
    filter(action => action.type === 'GET_FEE_PERCENTAGE'),
    distinctUntilChanged(),
    switchMap(action =>
      context$!.pipe(
        map(context => ({ context, action })),
        takeUntil(
          action$.pipe(
            filter(
              a => a.type === 'networkSwitch' || a.type === 'addressSwitch',
            ),
          ),
        ),
      ),
    ),
    switchMap(async ({ context, action }) => {
      if (context) {
        const { poolRealmAgent } = context;
        if (poolRealmAgent !== null) {
          if (action && action.payload) {
            const fee = await poolRealmAgent!.getFeePercentage(
              action.payload.paySymbol,
              action.payload.receiveSymbol,
            );
            return {
              type: 'app/setFeePercentage',
              payload: {
                amount: fee.toString(),
                paySymbol: action.payload.paySymbol,
                receiveSymbol: action.payload.receiveSymbol,
              },
            };
          }
        }
      }

      return {
        type: 'app/setFeePercentage',
        payload: {},
      };
    }),
  );
