import { Web3Provider } from '@ethersproject/providers';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import { Web3On } from '@jarvis-network/core-utils/dist/eth/web3-instance';
import {
  poolVersionsFromNetworkId,
  SupportedNetworkId,
  SupportedNetworkName,
} from '@jarvis-network/synthereum-config';
import { Provider } from 'ethers-multicall';
import { filter, firstValueFrom } from 'rxjs';
import Web3 from 'web3';

import { RealmAgent } from '../core/realm-agent';

import { loadPoolRealmAgent } from './core-frontend';
import { Context, context$ as contextStream$ } from './types';

export const createContext = async (
  provider: Web3Provider,
  connectorName: string | null,
): Promise<Context> => {
  const web3 = new Web3(
    provider.provider as any,
  ) as Web3On<SupportedNetworkName>;
  const nodeId = await web3.eth.net.getId();

  const context: Context = {
    web3, // web3.js
    networkId: null, //
    poolRealmAgent: null,
    poolRealm: null,
    agentAddress: null,
    ethersProvider: null,
    ethcallProvider: null,
    active: false,
    connectorName,
  };

  context.networkId = (await provider.getNetwork())
    .chainId as SupportedNetworkId;

  if (context.networkId !== nodeId) {
    throw new Error('Reload');
  }
  context.ethersProvider = provider; // ether.js
  context.ethcallProvider = new Provider(provider);
  await context.ethcallProvider.init();
  context.poolRealm = await loadPoolRealmAgent(context);
  return context;
};

export const updateAddress = async (
  agentAddress: AddressOn<SupportedNetworkName>,
): Promise<Context | null> => {
  if (agentAddress) {
    const { poolRealm, networkId, ...context } = (await firstValueFrom(
      contextStream$.pipe(filter(a => !!a && a.web3 !== null)),
    )) as Context;

    if (!poolRealm) return null;
    const poolRealmAgent = new RealmAgent(
      poolRealm,
      agentAddress,
      poolVersionsFromNetworkId[networkId!],
    );

    return {
      ...context,
      networkId,
      agentAddress,
      poolRealmAgent,
      poolRealm,
    };
  }
  return null;
};

export const AppEvents = [
  'networkSwitch',
  'addressSwitch',
  'transaction/metaMaskError',
  'transaction/cancel',
  'transaction/reset',
  'transaction/confirmed',
  'transaction/send',
  'transaction/validate',
  'transaction/metaMaskConfirmation',
  'approveTransaction/cancel',
  'approveTransaction/confirmed',
  'approveTransaction/send',
  'approveTransaction/metaMaskConfirmation',
];
