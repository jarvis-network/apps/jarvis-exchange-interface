import {
  ContractInfo,
  ContractInstance,
  TokenInstance,
} from '@jarvis-network/core-utils/dist/eth/contracts/types';
import { ToNetworkId } from '@jarvis-network/core-utils/dist/eth/networks';
import type {
  PoolVersion,
  priceFeed,
  SupportedNetworkName,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config';
import {
  ISynthereumLiquidityPool as PoolV5,
  ISynthereumPoolOnChainPriceFeed as PoolV4,
} from '@jarvisnetwork/synthereum-contracts/build/typechain';
import { BaseContract } from 'libs/core-utils/dist/eth/contracts/typechain/types';

export type PoolContract<Version extends PoolVersion> = Version extends 'v4'
  ? PoolV4
  : Version extends 'v5'
  ? PoolV5
  : never;

export interface SynthereumPool<
  Version extends PoolVersion,
  Net extends SupportedNetworkName = SupportedNetworkName,
  SynthSymbol extends SupportedSynthereumSymbol<Net> = SupportedSynthereumSymbol<Net>
> extends ContractInstance<Net, PoolContract<Version>> {
  networkId: ToNetworkId<Net>;
  versionId: Version;
  symbol: SynthSymbol;
  priceFeed: typeof priceFeed[SynthSymbol];
  collateralToken: TokenInstance<Net>;
  syntheticToken: TokenInstance<Net>;
  derivative?: ContractInfo<Net, BaseContract>;
}

export type PoolsForVersion<
  Version extends PoolVersion,
  Net extends SupportedNetworkName
> = {
  [SynthSymbol in SupportedSynthereumSymbol<Net>]?: SynthereumPool<
    Version,
    Net,
    SynthSymbol
  >;
};
