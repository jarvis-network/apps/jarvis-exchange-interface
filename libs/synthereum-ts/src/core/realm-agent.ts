import {
  assert,
  assertNotNull,
} from '@jarvis-network/core-utils/dist/base/asserts';
import { Amount, wei } from '@jarvis-network/core-utils/dist/base/big-number';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { t } from '@jarvis-network/core-utils/dist/base/meta';
import { AddressOn } from '@jarvis-network/core-utils/dist/eth/address';
import {
  getTokenAllowance,
  getTokenBalance,
  setMaxTokenAllowance,
  weiToTokenAmount,
} from '@jarvis-network/core-utils/dist/eth/contracts/erc20';
import {
  FullTxOptions,
  sendTx,
  sendTxAndLog,
  TxOptions,
} from '@jarvis-network/core-utils/dist/eth/contracts/send-tx';
import { TokenInstance } from '@jarvis-network/core-utils/dist/eth/contracts/types';
import {
  ExchangeSynthereumToken,
  PoolVersion,
  SupportedNetworkName,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config';
import {
  ISynthereumLiquidityPool,
  ISynthereumPoolOnChainPriceFeed,
  NonPayableTransactionObject,
} from '@jarvisnetwork/synthereum-contracts/build/typechain';
import type { PromiEvent, TransactionReceipt } from 'web3-core';

import { mapPools } from './pool-utils';
import { determineSide, isSupportedCollateral } from './realm-utils';
import { PoolsForVersion } from './types/pools';
import { SynthereumRealmWithWeb3 } from './types/realm';

interface BaseTxParams {
  collateral: Amount;
  txOptions?: TxOptions;
}

export interface MintParams<Net extends SupportedNetworkName>
  extends BaseTxParams {
  outputSynth: SupportedSynthereumSymbol<Net>;
  outputAmount: Amount;
}

interface ExchangeParams<Net extends SupportedNetworkName>
  extends BaseTxParams {
  inputSynth: SupportedSynthereumSymbol<Net>;
  inputAmount: Amount;
  outputSynth: SupportedSynthereumSymbol<Net>;
  outputAmount: Amount;
}

interface RedeemParams<Net extends SupportedNetworkName> extends BaseTxParams {
  inputSynth: SupportedSynthereumSymbol<Net>;
  inputAmount: Amount;
}

interface SwapResult {
  allowancePromise: Promise<true | TransactionReceipt>;
  txPromise: Promise<TransactionReceipt>;
  sendTx: Promise<{ promiEvent: PromiEvent<TransactionReceipt> }>;
}

export class RealmAgent<
  Net extends SupportedNetworkName = SupportedNetworkName
> {
  public readonly activePools: PoolsForVersion<PoolVersion, Net>;

  public readonly collateralToken: TokenInstance<SupportedNetworkName>;

  private readonly defaultTxOptions: FullTxOptions<Net>;

  constructor(
    public readonly realm: SynthereumRealmWithWeb3<Net>,
    public readonly agentAddress: AddressOn<Net>,
    public readonly poolVersion: PoolVersion,
  ) {
    this.activePools = assertNotNull(
      realm.pools![poolVersion],
      `realm.pools[${poolVersion}] is null`,
    );

    this.collateralToken = assertNotNull(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.activePools.jCHF?.collateralToken,
    );
    this.defaultTxOptions = {
      from: this.agentAddress,
      web3: this.realm.web3,
      confirmations: 1,
    };
  }

  async getFeePercentage(
    inputToken: ExchangeSynthereumToken,
    outputToken: ExchangeSynthereumToken,
  ): Promise<FPN> {
    const side = this.determineSide(inputToken, outputToken);
    if (side === 'unsupported') return new FPN(0.002);
    const token = (side === 'mint'
      ? outputToken
      : inputToken) as SupportedSynthereumSymbol<Net>;
    const pool = assertNotNull(
      this.activePools[token],
      'this.activePools[outputToken as SyntheticSymbol] is null',
    );
    let feePercentage: string;
    if (pool!.versionId === 'v5') {
      const poolV5 = pool!.instance as ISynthereumLiquidityPool;
      feePercentage = (await poolV5.methods
        .feePercentage()
        .call()
        .catch(err => console.log(err))) as any;
    } else {
      const poolV4 = pool!.instance as ISynthereumPoolOnChainPriceFeed;
      [[feePercentage]] = (await poolV4.methods
        .getFeeInfo()
        .call()
        .catch(err => console.log(err))) as any;
    }
    return new FPN(feePercentage, true);
  }

  collateralBalance(): Promise<Amount> {
    return getTokenBalance(this.collateralToken, this.agentAddress);
  }

  syntheticTokenBalanceOf(
    synthetic: SupportedSynthereumSymbol<Net>,
  ): Promise<Amount> {
    const asset = this.activePools[synthetic]!.syntheticToken;
    return getTokenBalance(asset, this.agentAddress);
  }

  getAllBalances(): Promise<[ExchangeSynthereumToken, Amount][]> {
    return Promise.all([
      (async (): Promise<[ExchangeSynthereumToken, Amount]> =>
        t(
          this.collateralToken.symbol as ExchangeSynthereumToken,
          await getTokenBalance(this.collateralToken, this.agentAddress),
        ) as [ExchangeSynthereumToken, Amount])(),
      ...mapPools(this.realm, this.poolVersion, async p =>
        t(
          p.symbol as SupportedSynthereumSymbol,
          await getTokenBalance(p.syntheticToken, this.agentAddress),
        ),
      ),
    ]);
  }

  mint({
    collateral,
    outputAmount,
    outputSynth,
    txOptions,
  }: MintParams<Net>): SwapResult {
    return this.universalExchange({
      inputToken: this.collateralToken.symbol as ExchangeSynthereumToken,
      outputToken: outputSynth,
      inputAmountWei: collateral,
      outputAmountWei: outputAmount,
      txOptions,
    });
  }

  exchange({
    inputSynth,
    outputSynth,
    inputAmount,
    outputAmount,
    txOptions,
  }: ExchangeParams<Net>): SwapResult {
    return this.universalExchange({
      inputToken: inputSynth,
      outputToken: outputSynth,
      inputAmountWei: inputAmount,
      outputAmountWei: outputAmount,
      txOptions,
    });
  }

  redeem({
    inputAmount,
    inputSynth,
    collateral,
    txOptions,
  }: RedeemParams<Net>): SwapResult {
    return this.universalExchange({
      inputToken: inputSynth,
      outputToken: this.collateralToken.symbol as ExchangeSynthereumToken,
      inputAmountWei: inputAmount,
      outputAmountWei: collateral,
      txOptions,
    });
  }

  private determineSide(
    input: ExchangeSynthereumToken,
    output: ExchangeSynthereumToken,
  ) {
    return determineSide(this.activePools, input, output);
  }

  private isCollateral(token: ExchangeSynthereumToken) {
    return isSupportedCollateral(this.activePools, token);
  }

  private static getExpiration(): number {
    const timeout = 4 * 3600;
    return ((Date.now() / 1000) | 0) + timeout;
  }

  // TODO: Make this public and remove mint/exchange/redeem functions
  private universalExchange({
    inputToken,
    outputToken,
    inputAmountWei,
    outputAmountWei,
    txOptions,
  }: {
    inputToken: ExchangeSynthereumToken;
    outputToken: ExchangeSynthereumToken;
    inputAmountWei: Amount;
    outputAmountWei: Amount;
    txOptions?: TxOptions;
  }): SwapResult {
    const side = this.determineSide(inputToken, outputToken);

    assert(
      side !== 'unsupported',
      'Unsupported exchange: ' +
        `input ${inputAmountWei} ${inputToken} -> output ${outputAmountWei} ${outputToken}`,
    );

    const inputPool =
      side === 'mint'
        ? null
        : assertNotNull(
            this.activePools[inputToken as SupportedSynthereumSymbol<Net>],
            'this.activePools[inputToken as SyntheticSymbol] is null',
          );

    const outputPool =
      side === 'redeem'
        ? null
        : assertNotNull(
            this.activePools[outputToken as SupportedSynthereumSymbol<Net>],
            'this.activePools[outputToken as SyntheticSymbol] is null',
          );

    const feePercentage = new FPN(0.04);

    const allowancePromise = this.ensureSufficientAllowanceFor(
      side === 'mint' ? outputPool!.collateralToken : inputPool!.syntheticToken,
      side === 'mint' ? outputPool!.address : inputPool!.address,
      inputAmountWei,
      txOptions,
    );

    const inputAmount = this.isCollateral(inputToken)
      ? weiToTokenAmount({
          wei: inputAmountWei,
          decimals:
            side === 'mint'
              ? outputPool!.collateralToken.decimals
              : inputPool!.collateralToken.decimals,
        })
      : inputAmountWei;

    // TODO: Refactor to make the code more extensible
    let tx: NonPayableTransactionObject<unknown>;
    if (side === 'mint') {
      if (outputPool!.versionId === 'v5') {
        const poolV5 = outputPool!.instance as ISynthereumLiquidityPool;
        tx = poolV5.methods[side]([
          `0x${wei(0).toString('hex')}`,
          `0x${inputAmount.toString('hex')}`,
          RealmAgent.getExpiration(),
          this.agentAddress,
        ]);
      } else {
        const poolV4 = outputPool!.instance as ISynthereumPoolOnChainPriceFeed;
        tx = poolV4.methods[side]([
          outputPool!.derivative!.address,
          `0x${wei(0).toString('hex')}`,
          `0x${inputAmount.toString('hex')}`,
          `0x${feePercentage.toString('hex')}`,
          RealmAgent.getExpiration(),
          this.agentAddress,
        ]);
      }
    } else if (side === 'exchange') {
      if (inputPool!.versionId === 'v5') {
        const poolV5 = inputPool!.instance as ISynthereumLiquidityPool;
        tx = poolV5.methods[side]([
          outputPool!.address,
          `0x${inputAmount.toString('hex')}`,
          `0x${wei(0).toString('hex')}`,
          RealmAgent.getExpiration(),
          this.agentAddress,
        ]);
      } else {
        const poolV4 = inputPool!.instance as ISynthereumPoolOnChainPriceFeed;
        tx = poolV4.methods[side]([
          inputPool!.derivative!.address,
          outputPool!.address,
          outputPool!.derivative!.address,
          `0x${inputAmount.toString('hex')}`,
          `0x${wei(0).toString('hex')}`,
          `0x${feePercentage.toString('hex')}`,
          RealmAgent.getExpiration(),
          this.agentAddress,
        ]);
      }
    } else if (side === 'redeem') {
      if (inputPool!.versionId === 'v5') {
        const poolV5 = inputPool!.instance as ISynthereumLiquidityPool;
        tx = poolV5.methods[side]([
          `0x${inputAmount.toString('hex')}`,
          `0x${wei(0).toString('hex')}`,
          RealmAgent.getExpiration(),
          this.agentAddress,
        ]);
      } else {
        const poolV4 = inputPool!.instance as ISynthereumPoolOnChainPriceFeed;
        tx = poolV4.methods[side]([
          inputPool!.derivative!.address,
          `0x${inputAmount.toString('hex')}`,
          `0x${wei(0).toString('hex')}`,
          `0x${feePercentage.toString('hex')}`,
          RealmAgent.getExpiration(),
          this.agentAddress,
        ]);
      }
    }

    const getSendTx = allowancePromise.then(() =>
      sendTx(tx, {
        ...this.defaultTxOptions,
        ...txOptions,
      }),
    );

    const txPromise = getSendTx.then(result => result.promiEvent);

    return {
      allowancePromise,
      txPromise,
      sendTx: getSendTx,
    };
  }

  private async ensureSufficientAllowanceFor(
    tokenInfo: TokenInstance<Net>,
    spender: AddressOn<Net>,
    necessaryAllowance: Amount,
    txOptions?: TxOptions,
  ) {
    const allowance = await getTokenAllowance(
      tokenInfo,
      this.agentAddress,
      spender,
    );

    if (allowance.lt(necessaryAllowance)) {
      const tx = setMaxTokenAllowance(tokenInfo, spender);
      return sendTxAndLog(tx, {
        ...this.defaultTxOptions,
        ...txOptions,
      });
    }
    return true;
  }
}
