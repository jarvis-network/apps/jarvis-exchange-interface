/* eslint-disable camelcase */
import { last } from '@jarvis-network/core-utils/dist/base/array-fp-utils';
import { throwError } from '@jarvis-network/core-utils/dist/base/asserts';
import { t } from '@jarvis-network/core-utils/dist/base/meta';
import {
  AddressOn,
  isAddress,
} from '@jarvis-network/core-utils/dist/eth/address';
import { getContract } from '@jarvis-network/core-utils/dist/eth/contracts/get-contract';
import type { ToNetworkId } from '@jarvis-network/core-utils/dist/eth/networks';
import type { Web3On } from '@jarvis-network/core-utils/dist/eth/web3-instance';
import { PoolVersion, poolVersionId } from '@jarvis-network/synthereum-config';
import {
  supportedSynthereumPairs,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config/dist/supported/synthereum-pairs';
import {
  ISynthereumPriceFeed_Abi,
  ISynthereumRegistry_Abi,
} from '@jarvisnetwork/synthereum-contracts/build/abi';
import SynthereumFinder_Abi from '@jarvisnetwork/synthereum-contracts/build/abi/SynthereumFinder.json';
import SynthereumRegistry_Abi from '@jarvisnetwork/synthereum-contracts/build/abi/SynthereumRegistry.json';
import { ISynthereumPoolOnChainPriceFeed } from '@jarvisnetwork/synthereum-contracts/build/typechain';
import { ethers } from 'ethers';
import { Contract, Provider } from 'ethers-multicall';

import {
  priceFeed as priceFeedAssets,
  SupportedNetworkId,
  SupportedNetworkName,
  synthereumConfig,
  SynthereumContractDependencies,
} from '../../../config';
import { loadPool } from '../../pool-utils';
import type { PoolsForVersion, SynthereumPool } from '../../types/pools';
import type { SynthereumRealmWithWeb3 } from '../../types/realm';
import { getTokenInfoSimple } from '../comman-realm';

export type PoolVersionsToLoad<Net extends SupportedNetworkName> = {
  [Version in PoolVersion]?: PoolsForVersion<Version, Net> | null;
};

const convertToSyntheticSymbol = (symbol: string): string =>
  `j${symbol.slice(0, 3)}` as SupportedSynthereumSymbol;

const getVersionsToLoad = <Net extends SupportedNetworkName>(
  netId: SupportedNetworkId,
  versionsToLoad?: PoolVersionsToLoad<Net>,
): PoolVersionsToLoad<Net> => {
  if (versionsToLoad === 'object' && versionsToLoad !== null) {
    return versionsToLoad;
  }
  if (versionsToLoad !== null) {
    if (versionsToLoad === 'v5') return { v5: null };
    if (versionsToLoad === 'v4') return { v4: null };
  }

  const versionList = Object.keys(
    synthereumConfig[netId as SupportedNetworkId].perVersionConfig,
  );
  return Object.assign({}, ...versionList.map(key => ({ [key]: null })));
};

/**
 * Load the default Synthereum Realm.
 * @param web3 Web3 instance to connect to
 * @param netId Integer representing one of the supported network ids
 */
export async function loadRealm<Net extends SupportedNetworkName>(
  web3: Web3On<Net>,
  ethersProvider: ethers.providers.Web3Provider,
  ethcallProvider: Provider,
  netId: ToNetworkId<Net>,
  versionsToLoad?: PoolVersionsToLoad<Net>,
): Promise<SynthereumRealmWithWeb3<Net>> {
  const config = synthereumConfig[netId as SupportedNetworkId]
    .contractsDependencies.synthereum as SynthereumContractDependencies<Net>;
  const versions = getVersionsToLoad(netId, versionsToLoad);

  try {
    return await loadCustomRealm(
      web3,
      ethersProvider,
      ethcallProvider,
      netId,
      config,
      versions,
    );
  } catch (error) {
    console.log(error);
    throw new Error('Unable to loadrealm,*********');
  }
}

/**
 * Load a custom Synthereum Realm, identified by the `config` parameter.
 * @param web3 Web3 instance to connect to
 * @param config Configuration object containing all of the contract
 * dependencies
 */
export async function loadCustomRealm<Net extends SupportedNetworkName>(
  web3: Web3On<Net>,
  ethersProvider: ethers.providers.Web3Provider,
  ethcallProvider: Provider,
  netId: ToNetworkId<Net>,
  config: SynthereumContractDependencies<Net>,
  versionsToLoad: PoolVersionsToLoad<Net>,
): Promise<SynthereumRealmWithWeb3<Net>> {
  try {
    const finderInstance = new Contract(config.finder, SynthereumFinder_Abi);

    const findPoolAndPriceFeed = [
      finderInstance.getImplementationAddress(
        ethers.utils.formatBytes32String('PoolRegistry'),
      ),
      finderInstance.getImplementationAddress(
        ethers.utils.formatBytes32String('PriceFeed'),
      ),
    ] as any;

    const [poolRegistryAddress, priceFeedAddress] = await ethcallProvider.all(
      findPoolAndPriceFeed,
    );
    if (poolRegistryAddress === undefined || priceFeedAddress === undefined) {
      throw new Error('Unable to found addresses');
    }

    const poolRegistry = getContract(
      web3,
      ISynthereumRegistry_Abi,
      poolRegistryAddress,
    );
    const priceFeed = getContract(
      web3,
      ISynthereumPriceFeed_Abi,
      priceFeedAddress,
    );

    const pr = new Contract(poolRegistryAddress, SynthereumRegistry_Abi);

    const collateralAddress = config.primaryCollateralToken.address;

    const loadAllPools = async <Version extends PoolVersion>(
      version: Version,
    ) => {
      const calls = [] as any;

      supportedSynthereumPairs[netId].forEach((symbol: string) => {
        calls.push(
          pr.getElements(
            convertToSyntheticSymbol(symbol),
            collateralAddress,
            poolVersionId(version),
          ),
        );
      });
      const poolsAddresses = await ethcallProvider.all(calls);

      const pairs = await Promise.all(
        supportedSynthereumPairs[netId].map(
          async (symbol: string, index: number) => {
            const info = await loadPoolInfo(
              web3,
              ethersProvider,
              ethcallProvider,
              netId,
              poolsAddresses[index],
              collateralAddress,
              version,
              convertToSyntheticSymbol(
                symbol,
              ) as SupportedSynthereumSymbol<Net>,
            );
            return t(convertToSyntheticSymbol(symbol), info);
          },
        ),
      );

      return Object.fromEntries(pairs.filter(x => !!x[1])) as PoolsForVersion<
        Version,
        Net
      >;
    };

    const pools: SynthereumRealmWithWeb3<Net>['pools'] = {};
    if (versionsToLoad.v4 === null) {
      pools.v4 = await loadAllPools('v4');
    }
    if (versionsToLoad.v5 === null) {
      pools.v5 = await loadAllPools('v5');
    }
    return {
      web3,
      netId,
      priceFeed,
      poolRegistry,
      pools,
    };
  } catch (error) {
    console.log(error);
    throw new Error('Unable to load Custom realm');
  }
}

export async function loadPoolInfo<
  Version extends PoolVersion,
  SynthSymbol extends SupportedSynthereumSymbol<Net>,
  Net extends SupportedNetworkName
>(
  web3: Web3On<Net>,
  ethersProvider: ethers.providers.Web3Provider,
  ethcallProvider: Provider,
  netId: ToNetworkId<Net>,
  poolRegistry: any,
  collateralAddress: AddressOn<Net>,
  version: Version,
  symbol: SynthSymbol,
): Promise<SynthereumPool<Version, Net, SynthSymbol> | null> {
  const versionId = poolVersionId(version);

  if (collateralAddress === undefined) {
    return null;
  }

  const poolAddresses = poolRegistry as string[];
  let derivative;
  if (poolAddresses === undefined) {
    return null;
  }
  if (poolAddresses.length === 0) {
    return null;
  }

  // Assume the last address in the array is the one we should interact with
  const lastPoolAddress = last(poolAddresses);

  if (!isAddress(lastPoolAddress)) {
    return null;
  }

  const poolAddress = lastPoolAddress as AddressOn<Net>;

  const { result: poolInstance } = await loadPool(web3, version, poolAddress);
  const poolAbi = poolInstance.instance.options.jsonInterface;

  const pInstance = new Contract(poolAddress, poolAbi as any);
  const calls = [pInstance.collateralToken(), pInstance.syntheticToken()];
  const [
    collateralTokenAddress,
    syntheticTokenAddress,
  ] = await ethcallProvider.all(calls);
  if (
    syntheticTokenAddress === undefined ||
    collateralTokenAddress === undefined
  ) {
    throw new Error('Unable to found addresses');
  }

  const st = getTokenInfoSimple(web3, syntheticTokenAddress);
  const ct = getTokenInfoSimple(web3, collateralTokenAddress);
  const tokenABI = st.instance.options.jsonInterface;
  const sInstance = new Contract(syntheticTokenAddress, tokenABI as any);
  const cInstance = new Contract(collateralTokenAddress, tokenABI as any);
  const tokenCalls = [
    sInstance.symbol(),
    sInstance.decimals(),
    cInstance.symbol(),
    cInstance.decimals(),
  ];
  const [sSymbol, sDecimals, cSymbol, cDecimals] = await ethcallProvider.all(
    tokenCalls,
  );

  if (versionId === 4) {
    const derivatives = (await (poolInstance.instance as ISynthereumPoolOnChainPriceFeed).methods
      .getAllDerivatives()
      .call()
      .catch(e => console.log(e, 'syntheticTokenAddress error'))) as string[];

    derivative = {
      address: derivatives[0] as AddressOn<Net>,
      connect: () => throwError('Unsupported function'),
    };
  }

  return {
    versionId: version,
    networkId: netId,
    priceFeed: priceFeedAssets[symbol],
    symbol,
    ...poolInstance,
    syntheticToken: {
      ...st,
      symbol: sSymbol,
      decimals: sDecimals,
    },
    collateralToken: {
      ...ct,
      symbol: cSymbol,
      decimals: cDecimals,
    },
    derivative,
  };
}
