import {
  PerTupleElement,
  typeCheck,
} from '@jarvis-network/core-utils/dist/base/meta';

// primary definitions:

export const allCollateralSymbols = [
  'USDC.e',
  'wXDAI',
  'USDC',
  'UMA',
  'BUSD',
] as const;
export const synthereumCollateralSymbols = typeCheck<CollateralSymbol[]>()([
  'wXDAI',
  'USDC.e',
  'USDC',
  'BUSD',
]);
export const selfMintingCollateralSymbols = typeCheck<CollateralSymbol[]>()([
  'wXDAI',
  'USDC.e',
  'UMA',
  'USDC',
  'BUSD',
]);

export const assetSymbols = [
  'AUD',
  'NZD',
  'BRL',
  'CAD',
  'CHF',
  'COP',
  'EUR',
  'GBP',
  'JPY',
  'NGN',
  'PHP',
  'SEK',
  'SGD',
  'SPX',
  'XAG',
  'XAU',
  'XTI',
  'ZAR',
  'CNY',
  'PLN',
  'MXN',
  'KRW',
] as const;

export type PairLike<
  AssetType extends string,
  CollateralType extends string,
  Separator extends string = '/'
> = `${AssetType}${Separator}${CollateralType}`;

export type CollateralSymbol = typeof allCollateralSymbols[number];
export type SynthereumCollateralSymbol = typeof synthereumCollateralSymbols[number];
export type SelfMintingCollateralSymbol = typeof selfMintingCollateralSymbols[number];
export type AssetSymbol = typeof assetSymbols[number];
export type SyntheticSymbol = `j${AssetSymbol}`;
export type AnySynthereumPair = `${SyntheticSymbol}/${SynthereumCollateralSymbol}`;
export type AnySelfMintingPair = `${SyntheticSymbol}/${SelfMintingCollateralSymbol}`;

export function isSupportedSynthereumCollateral(asset: unknown): boolean {
  try {
    return synthereumCollateralSymbols.includes(
      asset as SynthereumCollateralSymbol,
    );
  } catch {
    return false;
  }
}

export type SynthereumPair = SyntheticToForexPair<
  AnySynthereumPair,
  SynthereumCollateralSymbol,
  'USD',
  'j'
>;

export type SelfMintingPair =
  | SyntheticToForexPair<AnySelfMintingPair, 'UMA', 'UMA', 'j'>
  | SyntheticToForexPair<AnySelfMintingPair, 'wXDAI', 'USD', 'j'>
  | SyntheticToForexPair<AnySelfMintingPair, 'USDC.e', 'USD', 'j'>
  | SyntheticToForexPair<AnySelfMintingPair, 'USDC', 'USD', 'j'>
  | SyntheticToForexPair<AnySelfMintingPair, 'BUSD', 'USD', 'j'>;

export type PairToSynth<Pair extends string> = Pair extends `${infer Asset}${
  | 'USD'
  | SelfMintingCollateralSymbol}`
  ? `j${Asset}`
  : never;

export type SyntheticToForexPair<
  Pair extends string,
  Collateral extends string,
  CollateralReplacement extends string = Collateral,
  SyntheticPrefix extends string = 'j'
> = Pair extends `${SyntheticPrefix}${infer Asset}/${Collateral}`
  ? Asset extends Collateral
    ? never
    : `${Asset}${CollateralReplacement}`
  : never;

export type PerAsset<Config> = PerTupleElement<
  typeof allSyntheticSymbols,
  Config
>;

export type PriceFeed = PerAsset<string>;

type AdjustUsdc<Sym extends string> = Sym extends 'USD' ? 'USDC' : Sym;

// "EURUMA" | "EURUSD", "GBPUSD" | "GBPUMA" -> "jEUR/USDC" | "jEUR/UMA" | "jGBP/USDC" | "jGBP/UMA"
export type PairToExactPair<
  Pair extends string
> = Pair extends `${infer Asset}${SelfMintingCollateralSymbol | 'USD'}`
  ? Pair extends `${Asset}${infer Collateral}`
    ? `j${Asset}/${AdjustUsdc<Collateral>}`
    : never
  : never;

export type AssetOf<
  ExactPair extends string
> = ExactPair extends `j${infer Asset}/${SelfMintingCollateralSymbol}`
  ? Asset
  : never;

export type AssetFromSyntheticSymbol<
  ExactPair extends string
> = ExactPair extends `j${infer Asset}` ? Asset : never;

export type SyntheticSymbolOf<
  ExactPair extends string
> = ExactPair extends `j${infer Asset}/${SelfMintingCollateralSymbol}`
  ? `j${Asset}`
  : never;

export type CollateralOf<
  ExactPair extends string
> = ExactPair extends `j${AssetSymbol}/${infer Collateral}`
  ? Collateral
  : never;

// derived definitions:

export const primaryCollateralSymbol = synthereumCollateralSymbols[0];

export const allSyntheticSymbols = typeCheck<SyntheticSymbol[]>()([
  'jAUD',
  'jNZD',
  'jBRL',
  'jCAD',
  'jCHF',
  'jCOP',
  'jEUR',
  'jGBP',
  'jJPY',
  'jNGN',
  'jPHP',
  'jSEK',
  'jSGD',
  'jSPX',
  'jXAG',
  'jXAU',
  'jXTI',
  'jZAR',
  'jCNY',
  'jPLN',
  'jMXN',
  'jKRW',
] as const);

export const priceFeed = typeCheck<PriceFeed>()({
  jAUD: 'AUDUSD',
  jNZD: 'NZDUSD',
  jBRL: 'BRLUSD',
  jCAD: 'CADUSD',
  jCHF: 'CHFUSD',
  jCOP: 'COPUSD',
  jEUR: 'EURUSD',
  jGBP: 'GBPUSD',
  jJPY: 'JPYUSD',
  jNGN: 'NGNUSD',
  jPHP: 'PHPUSD',
  jSEK: 'SEKUSD',
  jSGD: 'SGDUSD',
  jSPX: 'SPXUSD',
  jXAG: 'XAGUSD',
  jXAU: 'XAUUSD',
  jXTI: 'XTIUSD',
  jZAR: 'ZARUSD',
  jCNY: 'CNYUSD',
  jPLN: 'PLNUSD',
  jMXN: 'MXNUSD',
  jKRW: 'KRWUSD',
} as const);

export const reversedPriceFeedPairs: string[] = [priceFeed.jCHF];
