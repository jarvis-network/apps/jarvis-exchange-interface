import {
  assertIncludes,
  throwError,
} from '@jarvis-network/core-utils/dist/base/asserts';

import { SupportedNetworkId } from './networks';

export const poolVersions = ['v4', 'v5'] as const;
export type PoolVersions = typeof poolVersions;
export type PoolVersion = PoolVersions[number];

export const poolVersionsFromNetworkId: {
  [key in SupportedNetworkId]: PoolVersion;
} = {
  1: 'v4',
  42: 'v4',
  56: 'v5',
  97: 'v5',
  137: 'v4',
  100: 'v5',
  43114: 'v5',
};

export function assertIsSupportedPoolVersion(x: unknown): PoolVersion {
  return assertIncludes(
    poolVersions,
    x,
    `'${x}' is not a supported pool version`,
  );
}

export function poolVersionId(version: PoolVersion) {
  return version === 'v4'
    ? 4
    : version === 'v5'
    ? 5
    : throwError(`'${version}' is not a supported pool version`);
}
