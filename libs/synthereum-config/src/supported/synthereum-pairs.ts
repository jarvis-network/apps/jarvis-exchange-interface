import { typeCheck } from '@jarvis-network/core-utils/dist/base/meta';
import { ToNetworkId } from '@jarvis-network/core-utils/dist/eth/networks';

import {
  PairLike,
  PairToSynth,
  SynthereumCollateralSymbol,
  SynthereumPair,
} from '../types/price-feed-symbols';

import {
  PerNetwork,
  SupportedNetworkId,
  SupportedNetworkName,
} from './networks';

export type NetworkPairs<
  PairType extends PairLike<string, string, ''>
> = PerNetwork<PairType[]>;

export const supportedSynthereumPairs = typeCheck<
  NetworkPairs<SynthereumPair>
>()({
  1: ['CHFUSD', 'EURUSD', 'GBPUSD'],
  42: ['CHFUSD', 'EURUSD', 'GBPUSD'],
  56: ['BRLUSD', 'CHFUSD', 'EURUSD', 'GBPUSD', 'NGNUSD', 'ZARUSD'],
  97: ['EURUSD', 'GBPUSD'],
  137: [
    'AUDUSD',
    'NZDUSD',
    'CADUSD',
    'CHFUSD',
    'COPUSD',
    'EURUSD',
    'GBPUSD',
    'JPYUSD',
    'PHPUSD',
    'SEKUSD',
    'SGDUSD',
    'CNYUSD',
    'PLNUSD',
    'MXNUSD',
    'KRWUSD',
    'NGNUSD',
  ],
  100: ['EURUSD', 'CHFUSD'],
  43114: ['CHFUSD', 'EURUSD'],
} as const);

export type SupportedSynthereumPairs = typeof supportedSynthereumPairs;

export type SupportedSynthereumPair<
  Net extends SupportedNetworkName = SupportedNetworkName
> = SupportedSynthereumPairs[ToNetworkId<Net>][number];

export type SupportedSynthereumSymbol<
  Net extends SupportedNetworkName = SupportedNetworkName
> = PairToSynth<SupportedSynthereumPair<Net>>;

export const supportedCollateral: PerNetwork<SynthereumCollateralSymbol> = {
  1: 'USDC',
  42: 'USDC',
  56: 'BUSD',
  97: 'BUSD',
  137: 'USDC',
  100: 'wXDAI',
  43114: 'USDC.e',
};

export function isSupportedCollateral(
  networkId: SupportedNetworkId,
  symbol?: string | null,
) {
  return supportedCollateral[networkId] === symbol;
}

export type SupportedSynthereumSymbolExact<
  Net extends SupportedNetworkName = SupportedNetworkName
> = keyof {
  [N in Net]: {
    [X in SupportedSynthereumSymbol<N>]: unknown;
  };
}[Net];

export type PerPair<Net extends SupportedNetworkName, Config> = {
  [Pair in SupportedSynthereumPairs[ToNetworkId<Net>][number]]: Config;
};

export type ExchangeSynthereumToken =
  | SupportedSynthereumSymbol
  | SynthereumCollateralSymbol;

export type PerSynthereumPair<
  Config,
  Net extends SupportedNetworkName = SupportedNetworkName
> = {
  [Pair in PairToSynth<
    SupportedSynthereumPairs[ToNetworkId<Net>][number]
  >]?: Config;
};
