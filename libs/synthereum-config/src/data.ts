import { toWeiString } from '@jarvis-network/core-utils/dist/base/big-number';
import { typeCheck } from '@jarvis-network/core-utils/dist/base/meta';
import { assertIsAddress as A } from '@jarvis-network/core-utils/dist/eth/address';

import {
  ChainLinkPriceAggregators,
  FixedPointNumber,
  SynthereumConfig,
  SyntheticTokenConfig,
  SyntheticTokens,
} from './types/config';
import { primaryCollateralSymbol } from './types/price-feed-symbols';

function toFixed(num: string): FixedPointNumber {
  return {
    rawValue: toWeiString(num),
  };
}

// Reference: https://docs.chain.link/docs/ethereum-addresses/
export const chainlinkAggregators = typeCheck<ChainLinkPriceAggregators>()({
  1: {
    EURUSD: A<1>('0xb49f677943BC038e9857d61E7d053CaA2C1734C1'),
    GBPUSD: A<1>('0x5c0Ab2d9b5a7ed9f470386e82BB36A3613cDd4b5'),
    CHFUSD: A<1>('0x449d117117838fFA61263B61dA6301AA2a88B13A'),
    XAUUSD: A<1>('0x214eD9Da11D2fbe465a6fc601a91E62EbEc1a0D6'),

    UMAETH: A<1>('0xf817B69EA583CAFF291E287CaE00Ea329d22765C'),
    ETHUSD: A<1>('0x5f4eC3Df9cbd43714FE2740f5E3616155c5b8419'),

    CADUSD: A<1>('0xa34317DB73e77d453b1B8d04550c44D10e981C8e'),
    JPYUSD: A<1>('0xBcE206caE7f0ec07b545EddE332A47C2F75bbeb3'),
    KRWUSD: A<1>('0x01435677FB11763550905594A16B645847C1d0F3'),
    NGNUSD: A<1>('0x3e59bc23ea3f39e69b5e662B6fC5e7e6D22B6914'),
    PHPUSD: A<1>('0x9481e7ad8BE6BbB22A8B9F7B9fB7588d1df65DF6'),
    ZARUSD: A<1>('0x438F81D95761d7036cd2617295827D9d01Cf593f'),
  },
  42: {
    EURUSD: A<42>('0x0c15Ab9A0DB086e062194c273CC79f41597Bbf13'),
    GBPUSD: A<42>('0x28b0061f44E6A9780224AA61BEc8C3Fcb0d37de9'),
    CHFUSD: A<42>('0xed0616BeF04D374969f302a34AE4A63882490A8C'),
    XAUUSD: A<42>('0xc8fb5684f2707C82f28595dEaC017Bfdf44EE9c5'),
    ETHUSD: A<42>('0x9326BFA02ADD2366b30bacB125260Af641031331'),
    JPYUSD: A<42>('0xD627B1eF3AC23F1d3e576FA6206126F3c1Bd0942'),
    KRWUSD: A<42>('0x9e465c5499023675051517E9Ee5f4C334D91e369'),

    // FAKE Address
    UMAETH: A<42>('0x0000000000000000000000000000000000000000'),
    CADUSD: A<42>('0x0000000000000000000000000000000000000000'),
    NGNUSD: A<42>('0x0000000000000000000000000000000000000000'),
    PHPUSD: A<42>('0x0000000000000000000000000000000000000000'),
    ZARUSD: A<42>('0x0000000000000000000000000000000000000000'),
  },
  56: {
    CHFUSD: A<56>('0x6f0Ac5bc75004adF21B50eE687002Bb1E2391a9F'),
    EURUSD: A<56>('0xCf9528141c84D65c30E7e10Cf0b6B381b607516c'),
    GBPUSD: A<56>('0xE24c6DcB4426e7A518A1C458a5609898C04aaE37'),
    NGNUSD: A<56>('0x681BfCd266BBE59fd73B930BA1b6956794C03eBF'),

    // FAKE Address
    CADUSD: A<56>('0x0000000000000000000000000000000000000000'),
    JPYUSD: A<56>('0x0000000000000000000000000000000000000000'),
    PHPUSD: A<56>('0x0000000000000000000000000000000000000000'),
    ZARUSD: A<56>('0x0000000000000000000000000000000000000000'),
    UMAETH: A<56>('0x0000000000000000000000000000000000000000'),
    XAUUSD: A<56>('0x0000000000000000000000000000000000000000'),
    ETHUSD: A<56>('0x0000000000000000000000000000000000000000'),
    KRWUSD: A<56>('0x0000000000000000000000000000000000000000'),
  },
  97: {
    EURUSD: A<97>('0x298778fB6ea99191148d22b7aDd5B822aC89DDA9'),
    GBPUSD: A<97>('0xD435c1fEDDAb200868025b6Cc047479Cd74e72b3'),

    // FAKE Address
    CHFUSD: A<97>('0x0000000000000000000000000000000000000000'),
    NGNUSD: A<97>('0x0000000000000000000000000000000000000000'),
    CADUSD: A<97>('0x0000000000000000000000000000000000000000'),
    JPYUSD: A<97>('0x0000000000000000000000000000000000000000'),
    PHPUSD: A<97>('0x0000000000000000000000000000000000000000'),
    ZARUSD: A<97>('0x0000000000000000000000000000000000000000'),
    UMAETH: A<97>('0x0000000000000000000000000000000000000000'),
    XAUUSD: A<97>('0x0000000000000000000000000000000000000000'),
    ETHUSD: A<97>('0x0000000000000000000000000000000000000000'),
    KRWUSD: A<97>('0x0000000000000000000000000000000000000000'),
  },
  137: {
    EURUSD: A<137>('0x73366Fe0AA0Ded304479862808e02506FE556a98'),
    GBPUSD: A<137>('0x099a2540848573e94fb1Ca0Fa420b00acbBc845a'),
    CHFUSD: A<137>('0xc76f762CedF0F78a439727861628E0fdfE1e70c2'),
    XAUUSD: A<137>('0x0C466540B2ee1a31b441671eac0ca886e051E410'),
    PHPUSD: A<137>('0x218231089Bebb2A31970c3b77E96eCfb3BA006D1'),
    SGDUSD: A<137>('0x8CE3cAc0E6635ce04783709ca3CC4F5fc5304299'),
    CADUSD: A<137>('0xACA44ABb8B04D07D883202F99FA5E3c53ed57Fb5'),
    JPYUSD: A<137>('0xD647a6fC9BC6402301583C91decC5989d8Bc382D'),
    ETHUSD: A<137>('0xF9680D99D6C9589e2a93a78A04A279e509205945'),
    NZDUSD: A<137>('0xa302a0B8a499fD0f00449df0a490DedE21105955'),
    PLNUSD: A<137>('0xB34BCE11040702f71c11529D00179B2959BcE6C0'),
    MXNUSD: A<137>('0x171b16562EA3476F5C61d1b8dad031DbA0768545'),
    KRWUSD: A<137>('0x24B820870F726dA9B0D83B0B28a93885061dbF50'),
    NGNUSD: A<137>('0x0df812C4D675d155815B1216cE1da9e68f1b7050'),

    UMAETH: A<137>('0x0000000000000000000000000000000000000000'),
    ZARUSD: A<137>('0x0000000000000000000000000000000000000000'),
  },
  80001: {
    EURUSD: A<80001>('0x876e7aa32a6f71405bd3ba5151cc6a43d15bffe6'),
    GBPUSD: A<80001>('0x6f94eed129e6e29b19951e5b6f40c025d2348cf2'),
    CHFUSD: A<80001>('0x1d5cbe59b8166d35eaa309045c5dd34b453eb4f3'),
    XAUUSD: A<80001>('0xddb827c1b6b29c02532739a53d226a57381e719c'),
    ETHUSD: A<80001>('0x0715A7794a1dc8e42615F059dD6e406A6594651A'),

    UMAETH: A<80001>('0x0000000000000000000000000000000000000000'),
    CADUSD: A<80001>('0x0000000000000000000000000000000000000000'),
    NGNUSD: A<80001>('0x0000000000000000000000000000000000000000'),
    PHPUSD: A<80001>('0x0000000000000000000000000000000000000000'),
    ZARUSD: A<80001>('0x0000000000000000000000000000000000000000'),
  },
  100: {
    EURUSD: A<100>('0x298778fB6ea99191148d22b7aDd5B822aC89DDA9'),
    CHFUSD: A<100>('0xFb00261Af80ADb1629D3869E377ae1EEC7bE659F'),

    // FAKE Address
    GBPUSD: A<100>('0x0000000000000000000000000000000000000000'),
    NGNUSD: A<100>('0x0000000000000000000000000000000000000000'),
    CADUSD: A<100>('0x0000000000000000000000000000000000000000'),
    JPYUSD: A<100>('0x0000000000000000000000000000000000000000'),
    PHPUSD: A<100>('0x0000000000000000000000000000000000000000'),
    ZARUSD: A<100>('0x0000000000000000000000000000000000000000'),
    UMAETH: A<100>('0x0000000000000000000000000000000000000000'),
    XAUUSD: A<100>('0x0000000000000000000000000000000000000000'),
    ETHUSD: A<100>('0x0000000000000000000000000000000000000000'),
    KRWUSD: A<100>('0x0000000000000000000000000000000000000000'),
  },
  43114: {
    CHFUSD: A<43114>('0xc76f762CedF0F78a439727861628E0fdfE1e70c2'),
    EURUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    GBPUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    XAUUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    PHPUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    SGDUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    CADUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    JPYUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    ETHUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    UMAETH: A<43114>('0x0000000000000000000000000000000000000000'),
    KRWUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    NGNUSD: A<43114>('0x0000000000000000000000000000000000000000'),
    ZARUSD: A<43114>('0x0000000000000000000000000000000000000000'),
  },
} as const);

const syntheticTokens = typeCheck<SyntheticTokens>()({
  jEUR: {
    syntheticName: 'Jarvis Synthetic Euro',
    syntheticSymbol: 'jEUR',
    priceFeedIdentifier: 'EURUSD',
    collateralRequirement: '1100000000000000000',
    startingCollateralization: '1819350',
    minSponsorTokens: '0',
    isContractAllowed: true,
  },
  jCHF: {
    syntheticName: 'Jarvis Synthetic Swiss Franc',
    syntheticSymbol: 'jCHF',
    priceFeedIdentifier: 'CHFUSD',
    collateralRequirement: '1100000000000000000',
    startingCollateralization: '2079000',
    minSponsorTokens: '0',
    isContractAllowed: true,
  },
  jGBP: {
    syntheticName: 'Jarvis Synthetic British Pound',
    syntheticSymbol: 'jGBP',
    priceFeedIdentifier: 'GBPUSD',
    collateralRequirement: '1100000000000000000',
    startingCollateralization: '2052000',
    minSponsorTokens: '0',
    isContractAllowed: true,
  },
} as const);

const jAUD = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Australian Dollar',
  syntheticSymbol: 'jAUD',
  priceFeedIdentifier: 'AUDUSD',
  collateralRequirement: '1025000000000000000',
  startingCollateralization: '789030',
  minSponsorTokens: '0',
  isContractAllowed: true,
});
const jPHP = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Philippine Peso',
  syntheticSymbol: 'jPHP',
  priceFeedIdentifier: 'PHPUSD',
  collateralRequirement: '1050000000000000000',
  startingCollateralization: '24490',
  minSponsorTokens: '0',
  isContractAllowed: true,
});
const jSGD = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Singapore Dollar',
  syntheticSymbol: 'jSGD',
  priceFeedIdentifier: 'SGDUSD',
  collateralRequirement: '1050000000000000000',
  startingCollateralization: '920375',
  minSponsorTokens: '0',
  isContractAllowed: true,
});
const jCNY = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Chinese Yuan',
  syntheticSymbol: 'jCNY',
  priceFeedIdentifier: 'CNYUSD',
  collateralRequirement: '1025000000000000000',
  startingCollateralization: '174130',
  minSponsorTokens: '0',
  isContractAllowed: true,
});
const jCAD = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Canadian Dollar',
  syntheticSymbol: 'jCAD',
  priceFeedIdentifier: 'CADUSD',
  collateralRequirement: '1025000000000000000',
  startingCollateralization: '889900',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jNGN = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Nigerian Naira',
  syntheticSymbol: 'jNGN',
  priceFeedIdentifier: 'NGNUSD',
  collateralRequirement: '1200000000000000000',
  startingCollateralization: '2696355000',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jJPY = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Japanese Yen',
  syntheticSymbol: 'jJPY',
  priceFeedIdentifier: 'JPYUSD',
  collateralRequirement: '1025000000000000000',
  startingCollateralization: '9527',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jSEK = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Swedish Krona',
  syntheticSymbol: 'jSEK',
  priceFeedIdentifier: 'SEKUSD',
  collateralRequirement: '1025000000000000000',
  startingCollateralization: '121220',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jCOP = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Colombian Peso',
  syntheticSymbol: 'jCOP',
  priceFeedIdentifier: 'SEKUSD',
  collateralRequirement: '1025000000000000000',
  startingCollateralization: '309',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jZAR = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic South African Rand',
  syntheticSymbol: 'jZAR',
  priceFeedIdentifier: 'ZARUSD',
  collateralRequirement: '1050000000000000000',
  startingCollateralization: '2696355000',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jBRL = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Brazilian Real',
  syntheticSymbol: 'jBRL',
  priceFeedIdentifier: 'BRLUSD',
  collateralRequirement: '1050000000000000000',
  startingCollateralization: '2696355000',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jNZD = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic New Zealand Dollar',
  syntheticSymbol: 'jNZD',
  priceFeedIdentifier: 'NZDUSD',
  collateralRequirement: '1050000000000000000',
  startingCollateralization: '843500',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jPLN = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Polish Zloty',
  syntheticSymbol: 'jPLN',
  priceFeedIdentifier: 'PLNUSD',
  collateralRequirement: '1025000000000000000',
  startingCollateralization: '248490',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jMXN = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Mexican Peso',
  syntheticSymbol: 'jMXN',
  priceFeedIdentifier: 'MXNUSD',
  collateralRequirement: '1050000000000000000',
  startingCollateralization: '62663',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

const jKRW = typeCheck<SyntheticTokenConfig>()({
  syntheticName: 'Jarvis Synthetic Korean Won',
  syntheticSymbol: 'jKRW',
  priceFeedIdentifier: 'KRWUSD',
  collateralRequirement: '1050000000000000000',
  startingCollateralization: '941',
  minSponsorTokens: '0',
  isContractAllowed: true,
});

export const synthereumConfig = typeCheck<SynthereumConfig>()({
  '1': {
    fees: {
      feePercentage: toWeiString('0.002'),
      feeRecipients: [
        A<1>('0x8eF00583bAa186094D9A34a0A4750C1D1BB86831'),
        A<1>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
      ],
      feeProportions: [50, 50],
    },
    roles: {
      admin: A<1>('0x128C8E20Dd4F2d8519dD605632660686bA35D212'),
      maintainer: A<1>('0x10D7C10A2F25bA6212968d8918eb687d589C6e0a'),
      liquidityProvider: A<1>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
      validator: A<1>('0x62Ca030EB6B3F9f0a8fd353d95C48a60763AAfF0'),
    },
    contractsDependencies: {
      uma: {
        identifierWhitelist: A<1>('0xcF649d9Da4D1362C4DAEa67573430Bd6f945e570'),
        finder: A<1>('0x40f941E48A552bF496B154Af6bf55725f18D77c3'),
      },
      synthereum: {
        finder: A<1>('0xD451dE78E297b496ee8a4f06dCF991C17580B452'),
        primaryCollateralToken: {
          address: A<1>('0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48'),
          symbol: primaryCollateralSymbol,
        },
        poolRegistry: A<1>('0xaB77024DdC68A3Fe942De8dDb0014738ED01A5e5'),
        selfMintingRegistry: A<1>('0x83D7AEee512DF37c694d36C983E0D4BdF12Cb6Bf'),
      },
      chainlink: chainlinkAggregators[1],
    },
    umaDerivativeConfig: {
      disputeBondPct: toFixed('0.05'),
      sponsorDisputeRewardPct: toFixed('0.5'),
      disputerDisputeRewardPct: toFixed('0.2'),
      withdrawalLiveness: 7200,
      liquidationLiveness: 7200,
      excessTokenBeneficiary: A<1>(
        '0x535aC9f9b46E515a3af364434061181a504D5bFb',
      ),
    },
    perVersionConfig: {
      v4: {
        version: 4,
        syntheticTokens,
      },
      v5: {
        version: 5,
        syntheticTokens: {},
      },
    },
  },
  '42': {
    fees: {
      feePercentage: toWeiString('0.002'),
      feeRecipients: [
        A<42>('0x61b5A06CE0FcdA6445fb454244Ce84ED64c41aCa'),
        A<42>('0x0C85fdB62CAC33F2bb7fE0366Ff9CBc65d3cdBDb'),
      ],
      feeProportions: [50, 50],
    },
    roles: {
      admin: A<42>('0x539E625172026F8d2AC50648a55871FA728618e8'),
      maintainer: A<42>('0x8Eadb31D981509c1a2e55111C5b9c56788c89486'),
      liquidityProvider: A<42>('0x61b5A06CE0FcdA6445fb454244Ce84ED64c41aCa'),
      validator: A<42>('0x61b5A06CE0FcdA6445fb454244Ce84ED64c41aCa'),
    },
    contractsDependencies: {
      uma: {
        identifierWhitelist: A<42>(
          '0xeF9c374b7976941fCAf5e501eaB531E430463fC6',
        ),
        finder: A<42>('0xeD0169a88d267063184b0853BaAAAe66c3c154B2'),
      },
      synthereum: {
        finder: A<42>('0xBeFaa064Ad33668C97D4C8C4d0237682B7D04E34'),
        primaryCollateralToken: {
          address: A<42>('0xe22da380ee6B445bb8273C81944ADEB6E8450422'),
          symbol: primaryCollateralSymbol,
        },
        poolRegistry: A<42>('0x6De2dd54A1FBBaCAd9a42eC289c5B371be2C9EF1'),
        selfMintingRegistry: A<42>(
          '0x61fa26046F9D5e47d15495Ef00efD9339E14E568',
        ),
      },
      chainlink: chainlinkAggregators[42],
    },
    umaDerivativeConfig: {
      disputeBondPct: toFixed('0.1'),
      sponsorDisputeRewardPct: toFixed('0.05'),
      disputerDisputeRewardPct: toFixed('0.2'),
      withdrawalLiveness: 7200,
      liquidationLiveness: 7200,
      excessTokenBeneficiary: A<42>(
        '0x49251bc21C3e3Af201F39AeEbF93474dA6a9A5E7',
      ),
    },
    perVersionConfig: {
      v4: {
        version: 4,
        syntheticTokens: {
          ...syntheticTokens,
          jPHP,
        },
      },
      v5: {
        version: 5,
        syntheticTokens: {},
      },
    },
  },
  '56': {
    fees: {
      feePercentage: toWeiString('0.002'),
      feeRecipients: [],
      feeProportions: [],
    },
    roles: {
      admin: A<56>('0x0000000000000000000000000000000000000000'),
      maintainer: A<56>('0x0000000000000000000000000000000000000000'),
      liquidityProvider: A<56>('0x0000000000000000000000000000000000000000'),
      validator: A<56>('0x0000000000000000000000000000000000000000'),
    },
    contractsDependencies: {
      uma: {
        identifierWhitelist: A<56>(
          '0x0000000000000000000000000000000000000000',
        ),
        finder: A<56>('0x0000000000000000000000000000000000000000'),
      },
      synthereum: {
        finder: A<56>('0x8f0Bf7DD7d235A5d784Dd68f8e14E14964418EE5'),
        primaryCollateralToken: {
          address: A<56>('0xe9e7cea3dedca5984780bafc599bd69add087d56'),
          symbol: 'BUSD',
        },
        poolRegistry: A<56>('0x02c72Fc6C231eE0f0e313c966e2da5cb7a52B503'),
        selfMintingRegistry: A<56>(
          '0x0000000000000000000000000000000000000000',
        ),
      },
      chainlink: chainlinkAggregators[56],
    },
    umaDerivativeConfig: {
      disputeBondPct: toFixed('0.1'),
      sponsorDisputeRewardPct: toFixed('0.05'),
      disputerDisputeRewardPct: toFixed('0.2'),
      withdrawalLiveness: 7200,
      liquidationLiveness: 7200,
      excessTokenBeneficiary: A<56>(
        '0x0000000000000000000000000000000000000000',
      ),
    },
    perVersionConfig: {
      v4: {
        version: 4,
        syntheticTokens: {},
      },
      v5: {
        version: 5,
        syntheticTokens: {
          ...syntheticTokens,
          jNGN,
          jZAR,
          jBRL,
        },
      },
    },
  },
  '97': {
    fees: {
      feePercentage: toWeiString('0.002'),
      feeRecipients: [],
      feeProportions: [],
    },
    roles: {
      admin: A<97>('0x0000000000000000000000000000000000000000'),
      maintainer: A<97>('0x0000000000000000000000000000000000000000'),
      liquidityProvider: A<97>('0x0000000000000000000000000000000000000000'),
      validator: A<97>('0x0000000000000000000000000000000000000000'),
    },
    contractsDependencies: {
      uma: {
        identifierWhitelist: A<97>(
          '0x0000000000000000000000000000000000000000',
        ),
        finder: A<97>('0x0000000000000000000000000000000000000000'),
      },
      synthereum: {
        finder: A<97>('0xf50eBF877FB54010809027cbe83D04846e16af19'),
        primaryCollateralToken: {
          address: A<97>('0x78867BbEeF44f2326bF8DDd1941a4439382EF2A7'),
          symbol: 'BUSD',
        },
        poolRegistry: A<97>('0x53A7db11F8F7aBD37BA6F275b8eb15Cecb0fe397'),
        selfMintingRegistry: A<97>(
          '0x0000000000000000000000000000000000000000',
        ),
      },
      chainlink: chainlinkAggregators[97],
    },
    umaDerivativeConfig: {
      disputeBondPct: toFixed('0.1'),
      sponsorDisputeRewardPct: toFixed('0.05'),
      disputerDisputeRewardPct: toFixed('0.2'),
      withdrawalLiveness: 7200,
      liquidationLiveness: 7200,
      excessTokenBeneficiary: A<97>(
        '0x0000000000000000000000000000000000000000',
      ),
    },
    perVersionConfig: {
      v4: {
        version: 4,
        syntheticTokens: {},
      },
      v5: {
        version: 5,
        syntheticTokens: {
          jEUR: syntheticTokens.jEUR,
          jGBP: syntheticTokens.jGBP,
        },
      },
    },
  },
  '137': {
    fees: {
      feePercentage: toWeiString('0.001'),
      feeRecipients: [
        A<137>('0x8eF00583bAa186094D9A34a0A4750C1D1BB86831'),
        A<137>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
      ],
      feeProportions: [50, 50],
    },
    roles: {
      admin: A<137>('0x8a73fdA882601C4B84B0C52D7d85E4BA46357ca1'),
      maintainer: A<137>('0x05Bd62e8Be770A03C0Da0eC3033cB637331F0941'),
      liquidityProvider: A<137>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
      validator: A<137>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
    },
    contractsDependencies: {
      uma: {
        identifierWhitelist: A<137>(
          '0x2271a5E74eA8A29764ab10523575b41AA52455f0',
        ),
        finder: A<137>('0x09aea4b2242abC8bb4BB78D537A67a245A7bEC64'),
      },
      synthereum: {
        finder: A<137>('0x43a98e5C4A7F3B7f11080fc9D58b0B8A80cA954e'),
        primaryCollateralToken: {
          address: A<137>('0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174'),
          symbol: primaryCollateralSymbol,
        },
        poolRegistry: A<137>('0xdCE12741DF9d2CcF2A8bB611684C8151De91a7d2'),
        selfMintingRegistry: A<137>(
          '0x0000000000000000000000000000000000000000',
        ),
      },
      chainlink: chainlinkAggregators[137],
    },
    umaDerivativeConfig: {
      disputeBondPct: toFixed('0.05'),
      sponsorDisputeRewardPct: toFixed('0.05'),
      disputerDisputeRewardPct: toFixed('0.2'),
      withdrawalLiveness: 86400,
      liquidationLiveness: 86400,
      excessTokenBeneficiary: A<137>(
        '0x8ef00583baa186094d9a34a0a4750c1d1bb86831',
      ),
    },
    perVersionConfig: {
      v4: {
        version: 4,
        syntheticTokens: {
          ...syntheticTokens,
          jAUD,
          jCAD,
          jCOP,
          jJPY,
          jPHP,
          jSEK,
          jSGD,
          jCNY,
          jNZD,
          jPLN,
          jMXN,
          jKRW,
          jNGN,
        },
      },
      v5: {
        version: 5,
        syntheticTokens: {},
      },
    },
  },
  '100': {
    fees: {
      feePercentage: toWeiString('0.002'),
      feeRecipients: [
        A<100>('0x8eF00583bAa186094D9A34a0A4750C1D1BB86831'),
        A<100>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
      ],
      feeProportions: [50, 50],
    },
    roles: {
      admin: A<100>('0xb5c54f57D7F3eC6367Db4fb5A08a1174797E221c'),
      maintainer: A<100>('0xd874E1B9aCe88BFaf2D5B9859164ac1001b40303'),
      liquidityProvider: A<100>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
      validator: A<100>('0x0000000000000000000000000000000000000000'),
    },
    contractsDependencies: {
      uma: {
        identifierWhitelist: A<100>(
          '0x0000000000000000000000000000000000000000',
        ),
        finder: A<100>('0x0000000000000000000000000000000000000000'),
      },
      synthereum: {
        finder: A<100>('0x80d629cf2D775cB9b97c4A95Fe2269e0E8459d3A'),
        primaryCollateralToken: {
          address: A<100>('0xe91D153E0b41518A2Ce8Dd3D7944Fa863463a97d'),
          symbol: 'wXDAI',
        },
        poolRegistry: A<100>('0x43a98e5C4A7F3B7f11080fc9D58b0B8A80cA954e'),
        selfMintingRegistry: A<100>(
          '0x0000000000000000000000000000000000000000',
        ),
      },
      chainlink: chainlinkAggregators[100],
    },
    umaDerivativeConfig: {
      disputeBondPct: toFixed('0.1'),
      sponsorDisputeRewardPct: toFixed('0.05'),
      disputerDisputeRewardPct: toFixed('0.2'),
      withdrawalLiveness: 7200,
      liquidationLiveness: 7200,
      excessTokenBeneficiary: A<100>(
        '0x0000000000000000000000000000000000000000',
      ),
    },
    perVersionConfig: {
      v4: {
        version: 4,
        syntheticTokens: {},
      },
      v5: {
        version: 5,
        syntheticTokens: {
          jEUR: syntheticTokens.jEUR,
          jCHF: syntheticTokens.jCHF,
        },
      },
    },
  },
  '43114': {
    fees: {
      feePercentage: toWeiString('0.002'),
      feeRecipients: [
        A<43114>('0x38329F26a075D53324EFd6e5F53869354Fa1cfBc'),
        A<43114>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
      ],
      feeProportions: [50, 50],
    },
    roles: {
      admin: A<43114>('0x20c7ec810e723c8c486b9345b1A6D23e615c946c'),
      maintainer: A<43114>('0xe3F130DcECF408bCc679e4FE269FbcDE6aC86d17'),
      liquidityProvider: A<43114>('0xc31249BA48763dF46388BA5C4E7565d62ed4801C'),
      validator: A<43114>('0x0000000000000000000000000000000000000000'),
    },
    contractsDependencies: {
      uma: {
        identifierWhitelist: A<43114>(
          '0x2aA25770E92DE3D61544E1c5245e8C968DdC34C5',
        ),
        finder: A<43114>('0x02c72Fc6C231eE0f0e313c966e2da5cb7a52B503'),
      },
      synthereum: {
        finder: A<43114>('0x02c72Fc6C231eE0f0e313c966e2da5cb7a52B503'),
        primaryCollateralToken: {
          address: A<43114>('0xA7D7079b0FEaD91F3e65f86E8915Cb59c1a4C664'),
          symbol: 'USDC.e',
        },
        poolRegistry: A<43114>('0x8FEceC5629EED60D18Fd3438aae4a8E69723D190'),
        selfMintingRegistry: A<43114>(
          '0x0000000000000000000000000000000000000000',
        ),
      },
      chainlink: chainlinkAggregators[43114],
    },
    umaDerivativeConfig: {
      disputeBondPct: toFixed('0.1'),
      sponsorDisputeRewardPct: toFixed('0.05'),
      disputerDisputeRewardPct: toFixed('0.2'),
      withdrawalLiveness: 7200,
      liquidationLiveness: 7200,
      excessTokenBeneficiary: A<43114>(
        '0x0000000000000000000000000000000000000000',
      ),
    },
    perVersionConfig: {
      v4: {
        version: 4,
        syntheticTokens: {},
      },
      v5: {
        version: 5,
        syntheticTokens: {
          jCHF: syntheticTokens.jCHF,
          jEUR: syntheticTokens.jEUR,
        },
      },
    },
  },
} as const);
