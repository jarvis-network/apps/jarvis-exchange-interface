import BUSD from './icons/BUSD.svg';
import jAUD from './icons/jAUD.svg';
import jBGN from './icons/jBGN.svg';
import jBRL from './icons/jBRL.svg';
import jCAD from './icons/jCAD.svg';
import jCHF from './icons/jCHF.svg';
import jCOP from './icons/jCOP.svg';
import jEUR from './icons/jEUR.svg';
import jGBP from './icons/jGBP.svg';
import jJPY from './icons/jJPY.svg';
import jCNY from './icons/jCNY.svg';
import jNGN from './icons/jNGN.svg';
import jPHP from './icons/jPHP.svg';
import jSEK from './icons/jSEK.svg';
import jSGD from './icons/jSGD.svg';
import jTRY from './icons/jTRY.svg';
import jXAU from './icons/jXAU.svg';
import jZAR from './icons/jZAR.svg';
import jNZD from './icons/jNZD.svg';
import jPLN from './icons/jPLN.svg';
import jMXN from './icons/jMXN.svg';
import jKRW from './icons/jKRW.svg';
import UMA from './icons/UMA.svg';
import USDC from './icons/USDC.svg';
import wXDAI from './icons/wXDAI.svg';

export const files = {
  BUSD,
  jAUD,
  jBGN,
  jBRL,
  jCAD,
  jCHF,
  jCOP,
  jEUR,
  jGBP,
  jJPY,
  jCNY,
  jMXN,
  jNGN,
  jPHP,
  jSEK,
  jSGD,
  jTRY,
  jXAU,
  jZAR,
  jNZD,
  jPLN,
  jKRW,
  UMA,
  USDC,
  wXDAI,
  'USDC.e': USDC,
};

export type FlagKeys = keyof typeof files;

export { default as questionMark } from './icons/question-mark.svg';
