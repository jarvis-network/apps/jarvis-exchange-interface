module.exports = {
  projects: [
    '<rootDir>/libs/core-utils',
    '<rootDir>/libs/synthereum-ts',
    '<rootDir>/libs/yield-farming',
    '<rootDir>/libs/atomic-swap',
    '<rootDir>/libs/jrt-investors',
    '<rootDir>/libs/synthereum-config',
  ],
};
