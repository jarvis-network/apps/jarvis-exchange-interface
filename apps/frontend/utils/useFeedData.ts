import {
  ExchangeSynthereumToken,
  priceFeed as priceFeedPairsMap,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-ts/dist/config';

import { useReduxSelector } from '@/state/useReduxSelector';
import { DataItem, PricePoint } from '@/state/initialState';
import { isSupportedSynthereumCollateral } from '@jarvis-network/synthereum-config';

const reversePricePoint = (data: PricePoint) => ({
  ...data,
  open: 1 / data.open,
  high: 1 / data.high,
  low: 1 / data.low,
  close: 1 / data.close,
});

export const useFeedData = (
  payAsset: ExchangeSynthereumToken,
  receiveAsset: ExchangeSynthereumToken,
): DataItem[] => {
  if (
    isSupportedSynthereumCollateral(payAsset) ||
    isSupportedSynthereumCollateral(receiveAsset)
  ) {
    if (!isSupportedSynthereumCollateral(payAsset)) {
      const pair = priceFeedPairsMap[payAsset as SupportedSynthereumSymbol];

      return useReduxSelector(state => state.prices.feed[pair] || []);
    }

    if (!isSupportedSynthereumCollateral(receiveAsset)) {
      const pair = priceFeedPairsMap[receiveAsset as SupportedSynthereumSymbol];

      return useReduxSelector(state =>
        (state.prices.feed[pair] || []).map(reversePricePoint),
      );
    }

    return [];
  }

  const payPair = priceFeedPairsMap[payAsset as SupportedSynthereumSymbol];
  const receivePair =
    priceFeedPairsMap[receiveAsset as SupportedSynthereumSymbol];

  const { payData, receiveData } = useReduxSelector(state => {
    const pay = state.prices.feed[payPair] || [];
    const receive = state.prices.feed[receivePair] || [];

    return {
      payData: pay,
      receiveData: receive,
    };
  });

  const times = payData.map(i => i.time);

  return times.map(time => {
    const pay = payData.find(i => i.time === time);
    const receive = receiveData.find(i => i.time === time);

    if (!pay || !receive) {
      return {
        time,
        history: true,
      };
    }

    return {
      ...pay,
      open: pay.open / receive.open,
      high: pay.high / receive.high,
      low: pay.low / receive.low,
      close: pay.close / receive.close,
    };
  });
};
