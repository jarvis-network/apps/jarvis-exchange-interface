import { Rate } from '@/state/initialState';
import { useReduxSelector } from '@/state/useReduxSelector';
import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config';

export const useRate = (
  inputSymbol: ExchangeSynthereumToken | null,
  outputSymbol: ExchangeSynthereumToken | null,
): Rate | null => {
  const { inputAsset, outputAsset } = useReduxSelector(state => ({
    inputAsset: state.assets[inputSymbol!],
    outputAsset: state.assets[outputSymbol!],
  }));

  // asset not found or price not yet loaded
  if (!inputAsset || !outputAsset || !inputAsset.price || !outputAsset.price) {
    return null;
  }
  return {
    rate: outputAsset.price.div(inputAsset.price),
  };
};
