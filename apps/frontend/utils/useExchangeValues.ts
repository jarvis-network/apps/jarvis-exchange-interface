import { useReduxSelector } from '@/state/useReduxSelector';
import { calcExchange } from '@/utils/calcExchange';
import { useGetFeePercentage } from '@/utils/useGetFeePercentage';
import { useRate } from '@/utils/useRate';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';

export const useExchangeValues = () => {
  const {
    base,
    pay,
    receive,
    assetPay,
    assetReceive,
    payBalance,
  } = useReduxSelector(state => ({
    ...state.exchange,
    assetPay: Object.values(state.assets).find(a =>
      a.symbol.startsWith(state.exchange.payAsset!),
    ),
    assetReceive: Object.values(state.assets).find(a =>
      a.symbol.endsWith(state.exchange.receiveAsset!),
    ),
    payBalance: state.wallet[state.exchange.payAsset!],
  }));

  const paySymbol = assetPay?.symbol || null;
  const receiveSymbol = assetReceive?.symbol || null;

  const feePercentage = useGetFeePercentage(paySymbol, receiveSymbol);
  const rate = useRate(paySymbol, receiveSymbol);
  let isMax = false;
  if (payBalance && payBalance.amount) {
    isMax = payBalance?.amount.eq(new FPN(pay));
  }

  const {
    payValue,
    receiveValue,
    netCollateral,
    grossCollateral,
    transactionCollateral,
  } = calcExchange({
    assetPay,
    assetReceive,
    base,
    pay,
    receive,
    fee: feePercentage,
    isMax,
  });

  let fee =
    grossCollateral && netCollateral
      ? grossCollateral.sub(netCollateral)
      : null;

  if (paySymbol === 'USDC' || paySymbol === 'USDC.e') {
    // mint
    if (base === 'pay' && grossCollateral) {
      fee = grossCollateral.mul(feePercentage);
    }
  }

  const payString = payValue?.format() || '';
  const receiveString = receiveValue?.format() || '';

  return {
    fee,
    base,
    pay,
    receive,
    paySymbol,
    receiveSymbol,
    assetPay,
    assetReceive,
    rate,
    payValue,
    payString,
    receiveValue,
    receiveString,
    transactionCollateral,
    feePercentage,
  };
};
