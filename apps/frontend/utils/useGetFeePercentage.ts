import { useReduxSelector } from '@/state/useReduxSelector';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { ExchangeSynthereumToken } from '@jarvis-network/synthereum-config';
import { useMemo } from 'react';
import { useDispatch } from 'react-redux';

export const useGetFeePercentage = (
  paySymbol: ExchangeSynthereumToken | null,
  receiveSymbol: ExchangeSynthereumToken | null,
) => {
  const dispatch = useDispatch();

  const feePercentage = useReduxSelector(state => state.app.feePercentage);

  useMemo(() => {
    (() => {
      if (paySymbol && receiveSymbol) {
        dispatch({
          type: 'GET_FEE_PERCENTAGE',
          payload: { paySymbol, receiveSymbol },
        });
      }
    })();
  }, [receiveSymbol, paySymbol]);

  return feePercentage ? FPN.fromWei(feePercentage) : new FPN(0.002);
};
