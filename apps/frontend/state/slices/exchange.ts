import { createSlice } from '@reduxjs/toolkit';

import {
  DEFAULT_PAY_ASSET,
  DEFAULT_RECEIVE_ASSET,
  DEFAULT_TRANSACTION_SPEED,
  initialAppState,
  State,
} from '@/state/initialState';
import { resetSwapAction } from '@/state/actions';
import { logoutAction, networkSwitchAction } from '@jarvis-network/app-toolkit';
import {
  poolVersionsFromNetworkId,
  isSupportedNetwork,
  supportedCollateral,
} from '@jarvis-network/synthereum-config';
import { assets } from '@/data/assets';

interface SetChooseAssetAction {
  payload: State['exchange']['chooseAssetActive'];
}

interface SetBaseAction {
  payload: State['exchange']['base'];
}

interface SetPayAction {
  payload: State['exchange']['pay'];
}

interface SetReceiveAction {
  payload: State['exchange']['receive'];
}

interface SetPayAssetAction {
  payload: State['exchange']['payAsset'];
}

interface SetReceiveAssetAction {
  payload: State['exchange']['receiveAsset'];
}

interface SetChartDays {
  payload: State['exchange']['chartDays'];
}

interface SetTransactionSpeed {
  payload: State['exchange']['transactionSpeed'];
}

const initialState = initialAppState.exchange;

const exchangeSlice = createSlice({
  name: 'exchange',
  initialState,
  reducers: {
    setChooseAsset(state, action: SetChooseAssetAction) {
      state.chooseAssetActive = action.payload;
    },
    setBase(state, action: SetBaseAction) {
      state.base = action.payload;
    },
    setPay(state, action: SetPayAction) {
      state.pay = action.payload;
    },
    setReceive(state, action: SetReceiveAction) {
      state.receive = action.payload;
    },
    setPayAsset(state, action: SetPayAssetAction) {
      if (action.payload === state.receiveAsset) {
        state.receiveAsset = state.payAsset;
      }
      state.payAsset = action.payload;
    },
    setReceiveAsset(state, action: SetReceiveAssetAction) {
      if (action.payload === state.payAsset) {
        state.payAsset = state.receiveAsset;
      }
      state.receiveAsset = action.payload;
    },
    invertRateInfo(state) {
      state.invertRateInfo = !state.invertRateInfo;
    },
    setChartDays(state, action: SetChartDays) {
      state.chartDays = action.payload;
    },
    setTransactionSpeed(state, action: SetTransactionSpeed) {
      state.transactionSpeed = action.payload;
    },
  },
  extraReducers: {
    [networkSwitchAction.type]: resetAssets,
    [resetSwapAction.type](state) {
      const { base, pay, receive } = initialState;

      return {
        ...state,
        base,
        pay,
        receive,
      };
    },
    [logoutAction.type](state) {
      return {
        ...state,
        transactionSpeed: DEFAULT_TRANSACTION_SPEED,
      };
    },
  },
});

function resetAssets(state: typeof initialState, action: any) {
  const { networkId }: { networkId: number } = action.payload;
  if (!isSupportedNetwork(networkId)) return;
  const assetsList = assets(networkId, poolVersionsFromNetworkId[networkId]);
  if (!assetsList.some(asset => asset.symbol === state.payAsset)) {
    state.payAsset = supportedCollateral[networkId];
    if (state.receiveAsset === DEFAULT_PAY_ASSET) {
      state.receiveAsset = DEFAULT_RECEIVE_ASSET;
    }
  }
  if (!assetsList.some(asset => asset.symbol === state.receiveAsset)) {
    state.receiveAsset = DEFAULT_RECEIVE_ASSET;
    if (state.payAsset === DEFAULT_RECEIVE_ASSET) {
      state.payAsset = supportedCollateral[networkId];
    }
  }
}

export const {
  setChooseAsset,
  setBase,
  setPay,
  setReceive,
  setPayAsset,
  setReceiveAsset,
  invertRateInfo,
  setChartDays,
  setTransactionSpeed,
} = exchangeSlice.actions;

export const { reducer } = exchangeSlice;
