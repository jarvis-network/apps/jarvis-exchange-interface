import { initialAppState, State } from '@/state/initialState';
import {
  addressSwitchAction,
  logoutAction,
  networkSwitchAction,
} from '@jarvis-network/app-toolkit';
import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import {
  ExchangeSynthereumToken,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-ts/dist/config';
import { RealmAgent } from '@jarvis-network/synthereum-ts/dist/core/realm-agent';
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

export interface WalletBalance {
  asset: ExchangeSynthereumToken;
  amount: FPN;
}

type PrivateState = {
  walletPrivate: {
    fetchingBalancesFor: { address: string; blockNumber: number } | null;
  };
};

type FetchWalletBalancesArgument = RealmAgent;
export const fetchWalletBalances = createAsyncThunk<
  WalletBalance[],
  FetchWalletBalancesArgument,
  { state: State & PrivateState }
>('wallet/fetch', async (realmAgent, { signal }) => {
  const balances = await realmAgent.getAllBalances();
  if (signal.aborted) throw new Error('fetchWalletBalances aborted');
  return balances.map(([asset, amount]) => ({
    asset,
    amount: FPN.fromWei(amount),
  }));
});

const initialState = initialAppState.wallet;

function resetState() {
  return initialState;
}

interface SetBalanceAction {
  payload: { symbol: SupportedSynthereumSymbol; balance: StringAmount }[];
}
const walletSlice = createSlice({
  name: 'wallet',
  initialState,
  reducers: {
    setBalances(state, action: SetBalanceAction) {
      console.log(action);

      if (action && action.payload) {
        for (const asset of action.payload) {
          state[asset.symbol] = {
            amount: FPN.fromWei(asset.balance),
          };
        }
      }
    },
  },
  extraReducers: {
    [logoutAction.type]: resetState,
    [addressSwitchAction.type]: resetState,
    [networkSwitchAction.type]: resetState,
  },
});

export const { reducer } = walletSlice;
