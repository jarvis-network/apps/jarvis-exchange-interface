import { initialAppState } from '@/state/initialState';
import { networkSwitchAction } from '@jarvis-network/app-toolkit/dist/sharedActions';
import { createSlice } from '@reduxjs/toolkit';

import { resetSwapAction } from '../actions';

interface BooleanAction {
  payload: boolean;
}

interface SetMobileTabAction {
  payload: number;
}

const appSlice = createSlice({
  name: 'app',
  initialState: initialAppState.app,
  reducers: {
    setAccountOverviewModalVisible(state, action: BooleanAction) {
      return {
        ...state,
        isAccountOverviewModalVisible: action.payload,
      };
    },
    setRecentActivityModalVisible(state, action: BooleanAction) {
      return {
        ...state,
        isRecentActivityModalVisible: action.payload,
      };
    },
    setFullScreenLoaderVisible(state, action: BooleanAction) {
      return {
        ...state,
        isFullScreenLoaderVisible: action.payload,
      };
    },
    setSwapLoaderVisible(state, action: BooleanAction) {
      return {
        ...state,
        isSwapLoaderVisible: action.payload,
      };
    },
    setAuthModalVisible(state, action: BooleanAction) {
      return {
        ...state,
        isAuthModalVisible: action.payload,
      };
    },
    setExchangeConfirmationVisible(state, action: BooleanAction) {
      return {
        ...state,
        isExchangeConfirmationVisible: action.payload,
      };
    },
    setWindowLoaded(state, action: BooleanAction) {
      return {
        ...state,
        isWindowLoaded: action.payload,
      };
    },
    setMobileTab(state, action: SetMobileTabAction) {
      return {
        ...state,
        mobileTab: action.payload,
      };
    },
    setExchangeSettingsVisible(state, action: BooleanAction) {
      return {
        ...state,
        areExchangeSettingsVisible: action.payload,
      };
    },
    setFeePercentage(state, action: any) {
      return {
        ...state,
        feePercentage: action.payload.amount,
      };
    },
  },
  extraReducers: {
    [resetSwapAction.type](state) {
      state.isSwapLoaderVisible = false;
      state.isExchangeConfirmationVisible = false;
    },
    [networkSwitchAction.type](state, action) {
      state.networkId = action.payload;
    },
  },
});

export const {
  setAccountOverviewModalVisible,
  setRecentActivityModalVisible,
  setFullScreenLoaderVisible,
  setSwapLoaderVisible,
  setAuthModalVisible,
  setExchangeConfirmationVisible,
  setWindowLoaded,
  setMobileTab,
  setExchangeSettingsVisible,
} = appSlice.actions;
export const { reducer } = appSlice;
