import { assets } from '@/data/assets';
import {
  addressSwitchAction,
  logoutAction,
  networkSwitchAction,
} from '@jarvis-network/app-toolkit/dist/sharedActions';
import { StringAmount } from '@jarvis-network/core-utils/dist/base/big-number';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import {
  isSupportedNetworkId,
  poolVersionsFromNetworkId,
  SupportedSynthereumSymbol,
} from '@jarvis-network/synthereum-config';
import { createSlice } from '@reduxjs/toolkit';

import { assetsList, DEFAULT_PAIRS, initialAppState } from '../initialState';

const initialState = initialAppState.assets;

interface SetCurrentPriceAction {
  payload: [SupportedSynthereumSymbol, StringAmount][];
}

const priceSlice = createSlice({
  name: 'assets',
  initialState,
  reducers: {
    setCurrentPrice(state, action: SetCurrentPriceAction) {
      console.log(action);
      if (action && action.payload) {
        for (const [symbol, price] of action.payload) {
          if (symbol in state) {
            state[symbol]!.price = price ? new FPN(price, true) : null;
          }
        }
      }
    },
  },
  extraReducers: {
    [addressSwitchAction.type]: resetAsset,
    [networkSwitchAction.type]: resetAsset,
    [logoutAction.type]() {
      return initialState;
    },
  },
});

function resetAsset(state: any, action: any) {
  const { networkId }: { networkId: number } = action.payload;
  let newState: assetsList = {};
  if (isSupportedNetworkId(networkId)) {
    const assetLists = assets(networkId, poolVersionsFromNetworkId[networkId]);
    newState = { ...DEFAULT_PAIRS };
    for (const asset of assetLists) {
      newState[asset.symbol] = asset;
    }
  } else {
    state = {};
  }
  return newState;
}

export const { setCurrentPrice } = priceSlice.actions;
export const { reducer } = priceSlice;
