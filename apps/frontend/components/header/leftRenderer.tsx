import React from 'react';
import {
  styled,
  getButtonStyles,
  ButtonModifierProps,
} from '@jarvis-network/ui';
import { useWeb3 } from '@jarvis-network/app-toolkit';

export declare const showMtpModal: any;

const Button = styled.a<ButtonModifierProps>(props =>
  getButtonStyles(props, props.theme),
);
const CustomButton = styled(Button)`
  @media screen and (max-width: 400px) {
    display: none;
  }
`;

const rand = (
  min: number,
  max: number, // min and max included
) => Math.floor(Math.random() * (max - min + 1) + min);

const render = () => {
  const { account: address, provider, chainId } = useWeb3();

  const hexToBase64 = (source: string) => {
    const symbols = source?.match(/.{2}/g)?.map(c => parseInt(c, 16));

    if (!symbols) {
      return '';
    }

    return btoa(String.fromCharCode(...symbols));
  };

  const handleSignMessage = (code: number) =>
    new Promise(resolve => {
      if (!provider) {
        return;
      }

      provider
        .getSigner(address)
        .signMessage(`MtPelerin-${code}`)
        .then(res => resolve(hexToBase64(res.slice(2))));
    });

  const selectedNetwork = (source: number | undefined) => {
    if (source === 137) {
      return 'matic_mainnet';
    }
    if (source === 56) {
      return 'bsc_mainnet';
    }
    if (source === 1) {
      return 'mainnet';
    }
    if (source === 100) {
      return 'xdai_mainnet';
    }
    if (source === 43114) {
      return 'avalanche_mainnet';
    }
    return 'matic_mainnet';
  };

  const handleBuyWithFiat = () => {
    if (!address) {
      showMtpModal({
        lang: 'en',
        tab: 'buy',
        bsc: 'EUR',
        bdc: 'jEUR',
        ssc: 'jEUR',
        sdc: 'EUR',
        crys: 'jAUD, jCAD, jCHF, jEUR, jGBP, jJPY, jSEK, jSGD, jZAR',
        net: 'matic_mainnet',
        nets:
          'avalanche_mainnet, bsc_mainnet, mainnet, matic_mainnet, xdai_mainnet',
        mylogo: 'https://app.jarvis.exchange/images/logo.svg',
        rfr: 'jarvis',
      });

      (document?.querySelector(
        '#MtPelerinModal.mtp-modal',
      ) as any).style.display = 'flex';

      return;
    }

    const code = rand(1000, 9999);

    handleSignMessage(code).then(hash => {
      showMtpModal({
        lang: 'en',
        tab: 'buy',
        bsc: 'EUR',
        bdc: 'jEUR',
        ssc: 'jEUR',
        sdc: 'EUR',
        crys: 'jAUD, jCAD, jCHF, jEUR, jGBP, jJPY, jSEK, jSGD, jZAR',
        net: selectedNetwork(chainId),
        nets: selectedNetwork(chainId),
        addr: address,
        code,
        hash,
        mylogo: 'https://app.jarvis.exchange/images/logo.svg',
        rfr: 'jarvis',
      });

      (document?.querySelector(
        '#MtPelerinModal.mtp-modal',
      ) as any).style.display = 'flex';
    });
  };

  return (
    <CustomButton size="m" onClick={handleBuyWithFiat}>
      Buy with fiat
    </CustomButton>
  );
};

export const leftRenderer = { render };
