import React from 'react';
import { styled, Tooltip } from '@jarvis-network/ui';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { useExchangeValues } from '@/utils/useExchangeValues';
import { useCoreObservables } from '@jarvis-network/app-toolkit/dist/CoreObservablesContext';
import { useBehaviorSubject } from '@jarvis-network/app-toolkit/dist/useBehaviorSubject';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  heigth: auto;
  border-radius: ${props => props.theme.borderRadius.m};
  background: ${props => props.theme.background.primary};
  padding: 15px;
`;

const Line = styled.div`
  padding: 7px 0;
  display: flex;
  font-size: ${props => props.theme.font.sizes.s};
  justify-content: space-between;
  align-items: center;

  :first-child {
    padding-top: 0;
  }

  :last-child {
    padding-bottom: 0;
  }
`;
const Key = styled.div``;
const Value = styled.div`
  text-align: right;
`;

const QuestionMark = styled.span`
  display: inline-block;
  width: ${props => props.theme.font.sizes.xxs};
  height: ${props => props.theme.font.sizes.xxs};
  color: #80dfff;
  font-size: ${props => props.theme.font.sizes.xxs};
  border-radius: 200px;
  border: 1px solid #80dfff;
  text-align: center;
  line-height: ${props => props.theme.font.sizes.xxs};
  margin-left: 3px;
  transform: translateY(-2px);

  &::before {
    content: '?';
  }
`;

export const FEES_BLOCK_HEIGHT_PX = 100;

export const Fees: React.FC = () => {
  const { fee, feePercentage } = useExchangeValues();
  const { synthereumRealmAgent$: realmAgent$ } = useCoreObservables();
  const collateralSymbol = useBehaviorSubject(realmAgent$)?.collateralToken
    .symbol;

  const providerFeeText = `A ${feePercentage
    .mul(new FPN(100))
    .format()}% provider fee is collected and split between Liquidity Provider and the protocol treasury`;

  return (
    <Container>
      <Line>
        <Key>
          Protocol fee
          <Tooltip tooltip={providerFeeText} position="top">
            <QuestionMark />
          </Tooltip>
        </Key>
        <Value>{fee ? `${fee?.format(5)} ${collateralSymbol}` : '---'}</Value>
      </Line>
    </Container>
  );
};
