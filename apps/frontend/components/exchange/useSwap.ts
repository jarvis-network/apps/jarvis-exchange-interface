import { wei } from '@jarvis-network/core-utils/dist/base/big-number';
import { useExchangeValues } from '@/utils/useExchangeValues';
import {
  useBehaviorSubject,
  useCoreObservables,
  useTransactionSpeedContext,
} from '@jarvis-network/app-toolkit';
import {
  SupportedSynthereumSymbol,
  isSupportedCollateral,
} from '@jarvis-network/synthereum-config';
import { useReduxSelector } from '@/state/useReduxSelector';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import { TransactionSpeed } from '@/state/initialState';

export const useSwap = () => {
  const agent = useBehaviorSubject(useCoreObservables().synthereumRealmAgent$);
  const networkId = agent?.realm.netId;
  const {
    paySymbol,
    payValue,
    receiveSymbol,
    receiveValue,
    transactionCollateral,
  } = useExchangeValues();
  const { transactionSpeed } = useReduxSelector(state => ({
    transactionSpeed: state.exchange.transactionSpeed,
  }));

  const transactionSpeedContext = useTransactionSpeedContext();

  if (!agent || paySymbol === receiveSymbol || !networkId) {
    // symbols should never be the same, but just in case..
    return null;
  }

  if (isSupportedCollateral(networkId, paySymbol)) {
    // mint
    return () => {
      const collateral = wei(transactionCollateral!.bn.toString(10));
      const outputAmount = wei(receiveValue!.bn.toString(10));
      const outputSynth = receiveSymbol as SupportedSynthereumSymbol;

      const { allowancePromise, txPromise, sendTx } = agent.mint({
        collateral,
        outputAmount,
        outputSynth,
        txOptions: {
          gasPrice: calculateGasPrice(
            transactionSpeedContext,
            transactionSpeed,
          ),
        },
      });

      txPromise.then(result => console.log('Minted!', result));

      return { allowancePromise, txPromise, sendTx };
    };
  }
  if (isSupportedCollateral(networkId, receiveSymbol)) {
    // redeem
    return () => {
      const collateral = wei(transactionCollateral!.bn.toString(10));
      const inputAmount = wei(payValue!.bn.toString(10));
      const inputSynth = paySymbol as SupportedSynthereumSymbol;

      const { allowancePromise, txPromise, sendTx } = agent.redeem({
        collateral,
        inputAmount,
        inputSynth,
        txOptions: {
          gasPrice: calculateGasPrice(
            transactionSpeedContext,
            transactionSpeed,
          ),
        },
      });

      txPromise.then(result => console.log('Redeem!', result));

      return { allowancePromise, txPromise, sendTx };
    };
  }
  // else: exchange

  return () => {
    const collateral = wei(transactionCollateral!.bn.toString(10));
    const inputAmount = wei(payValue!.bn.toString(10));
    const inputSynth = paySymbol as SupportedSynthereumSymbol;
    const outputAmount = wei(receiveValue!.bn.toString(10));
    const outputSynth = receiveSymbol as SupportedSynthereumSymbol;

    const { allowancePromise, txPromise, sendTx } = agent.exchange({
      collateral,
      inputAmount,
      inputSynth,
      outputAmount,
      outputSynth,
      txOptions: {
        gasPrice: calculateGasPrice(transactionSpeedContext, transactionSpeed),
      },
    });

    txPromise.then(result => console.log('Exchange!', result));

    return { allowancePromise, txPromise, sendTx };
  };
};

const oneGwei = FPN.fromWei(1000_000_000);

function calculateGasPrice(
  transactionSpeedContext: ReturnType<typeof useTransactionSpeedContext>,
  transactionSpeed: TransactionSpeed,
): string | undefined {
  return transactionSpeedContext.current
    ? oneGwei
        .mul(new FPN(transactionSpeedContext.current[transactionSpeed]))
        .toString(10)
    : undefined;
}
