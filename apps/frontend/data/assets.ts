import { SubscriptionPair } from '@/utils/priceFeed';
import { FPN } from '@jarvis-network/core-utils/dist/base/fixed-point-number';
import {
  supportedCollateral,
  SupportedNetworkId,
} from '@jarvis-network/synthereum-config';
import {
  ExchangeSynthereumToken,
  PoolVersion,
  synthereumConfig,
} from '@jarvis-network/synthereum-ts/dist/config';

export interface Asset {
  name: string;
  symbol: ExchangeSynthereumToken;
  pair: SubscriptionPair | null;
  price: FPN | null;
  decimals: number;
  type: 'forex' | 'crypto';
}

export interface AssetPair {
  input: Asset;
  output: Asset;
  name: string;
}

export const PRIMARY_STABLE_COIN_TEXT_SYMBOL = '$';

export const USDC: Asset = Object.freeze({
  name: 'USDC',
  symbol: 'USDC',
  pair: null,
  price: new FPN(1),
  decimals: 6,
  type: 'forex',
});

export const USDCe: Asset = Object.freeze({
  name: 'USDC.e',
  symbol: 'USDC.e',
  pair: null,
  price: new FPN(1),
  decimals: 6,
  type: 'forex',
});

export const wXDAI: Asset = Object.freeze({
  name: 'wXDAI',
  symbol: 'wXDAI',
  pair: null,
  price: new FPN(1),
  decimals: 6,
  type: 'forex',
});

export const BUSD: Asset = Object.freeze({
  name: 'BUSD',
  symbol: 'BUSD',
  pair: null,
  price: new FPN(1),
  decimals: 18,
  type: 'forex',
});

export interface AssetWithWalletInfo extends Asset {
  stableCoinValue: FPN | null;
  ownedAmount: FPN;
}

function* syntheticAssets(
  networkId: SupportedNetworkId,
  poolVersion: PoolVersion,
): Generator<Asset> {
  const config =
    synthereumConfig[networkId].perVersionConfig[poolVersion].syntheticTokens;
  for (const val in config) {
    if ((val as 'jEUR') in config) {
      const value = config[val as keyof typeof config];
      yield {
        name: value.syntheticName,
        symbol: value.syntheticSymbol,
        pair: value.priceFeedIdentifier,
        price: null,
        decimals: 18,
        type: 'forex',
      } as const;
    }
  }
}

const collaterals = {
  USDC,
  BUSD,
  wXDAI,
  'USDC.e': USDCe,
} as const;

export const assets = (
  networkId: SupportedNetworkId,
  poolVersion: PoolVersion,
): Asset[] => [
  collaterals[supportedCollateral[networkId]],
  ...syntheticAssets(networkId, poolVersion),
];
